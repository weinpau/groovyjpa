package org.pw.groovy.jpa;

import javax.persistence.EntityManager;

/**
 *
 * @author Paul Weinhold
 */
public class EntityManagerExtension {
 
    public static <E> GroovyDAO<E> getDAO(EntityManager self, Class<E> entityClass) {
        return new GroovyDAO<>(entityClass, self);
    }
    
}

package org.pw.groovy.jpa;


/**
 *
 * @author Paul Weinhold
 */
public final class IdentificationVariableCreator {

    private static long _identificationVariable;

    public static String createIdentificationVariable() {
        return "iv" + ++_identificationVariable;
    }
}

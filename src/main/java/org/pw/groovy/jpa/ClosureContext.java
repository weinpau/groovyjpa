package org.pw.groovy.jpa;

import groovy.lang.Binding;
import groovy.lang.Closure;
import groovy.lang.GroovyShell;
import groovy.lang.Reference;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import org.codehaus.groovy.ast.expr.Expression;

/**
 *
 * @author Paul Weinhold
 */
public class ClosureContext {

    private Closure closure;
    private Map<String, Object> localVariableContext;
    private GroovyShell groovyShell;

    public ClosureContext(Closure closure) {
        this.closure = closure;
    }

    public Object evaluate(Expression expression) {
        CodeVisitor visitor = new CodeVisitor();
        expression.visit(visitor);
        return evaluate(visitor.getCode());
    }

    public Object evaluate(String code) {
        try {
            return getGroovyShell().evaluate(code);
        } catch (Exception excep) {
            return null;
        }
    }

    public Map<String, Class> getLocalVariableContextTypes() {
        Map<String, Class> types = new HashMap<>();
        for (Map.Entry<String, Object> entry : getLocalVariableContext().entrySet()) {
            Class type = Object.class;
            if (entry.getValue() != null) {
                type = entry.getValue().getClass();
            }

            types.put(entry.getKey(), type);
        }
        return types;
    }

    public Map<String, Object> getLocalVariableContext() {
        localVariableContext = new HashMap<>();
        for (Field field : closure.getClass().getDeclaredFields()) {
            if (field.getType().equals(Reference.class)) {
                try {
                    field.setAccessible(true);
                    localVariableContext.put(field.getName(), ((Reference) field.get(closure)).get());
                } catch (IllegalArgumentException | IllegalAccessException ex) {
                }
            }
        }
        return localVariableContext;
    }

    private GroovyShell getGroovyShell() {
        if (groovyShell == null) {
            Binding b = new Binding(getLocalVariableContext());
            groovyShell = new GroovyShell(b);
        }
        return groovyShell;
    }
}

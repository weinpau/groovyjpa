package org.pw.groovy.jpa;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Paul Weinhold
 */
public class QueryParameters implements Iterable {

    private List parameters = new LinkedList();

    public void addParameter(Object parameter) {
        this.parameters.add(parameter);
    }

    public String getCurrentParamName() {
        return "?" + parameters.size();
    }

    @Override
    public Iterator iterator() {
        return parameters.iterator();
    }

    @Override
    public QueryParameters clone() {
        QueryParameters clone = new QueryParameters();
        clone.parameters = new LinkedList(this.parameters);
        return clone;
    }

    @Override
    public String toString() {
        return parameters.toString();
    }
}

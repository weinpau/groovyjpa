package org.pw.groovy.jpa;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import org.codehaus.groovy.ast.CodeVisitorSupport;
import org.codehaus.groovy.ast.expr.*;
import org.codehaus.groovy.syntax.Types;

/**
 *
 * @author Paul Weinhold
 */
public class CodeVisitor extends CodeVisitorSupport {

    private final StringBuffer buffer = new StringBuffer();
    private List<String> variables = new LinkedList<>();

    public String getCode() {
        return buffer.toString();
    }

    public List<String> getVariables() {
        return variables;
    }

    @Override
    public void visitBinaryExpression(BinaryExpression expression) {
        if (expression.getOperation().getType() == Types.LEFT_SQUARE_BRACKET) {
            expression.getLeftExpression().visit(this);
            buffer.append("[");
            expression.getRightExpression().visit(this);
            buffer.append("]");
        } else {
            buffer.append("(");
            expression.getLeftExpression().visit(this);
            buffer.append(" ");
            buffer.append(expression.getOperation().getText());
            buffer.append(" ");
            expression.getRightExpression().visit(this);
            buffer.append(")");
        }
    }

    @Override
    public void visitConstantExpression(ConstantExpression expression) {
        if (expression.getValue() == null) {
            buffer.append("null");
        } else if (expression.getValue().getClass().equals(String.class)) {
            buffer.append("'");
            buffer.append(expression.getValue().toString());
            buffer.append("'");
        } else {
            buffer.append(expression.getValue().toString());
        }
    }

    @Override
    public void visitArrayExpression(ArrayExpression expression) {
        buffer.append("[");
        boolean first = true;
        for (Expression ex : expression.getExpressions()) {
            if (first) {
                first = false;
            } else {
                buffer.append(", ");
            }
            ex.visit(this);
        }
        buffer.append("]");
    }

    @Override
    public void visitCastExpression(CastExpression expression) {
        buffer.append("(");
        buffer.append(expression.getType());
        buffer.append(") ");
        expression.getExpression().visit(this);
    }

    @Override
    public void visitClassExpression(ClassExpression expression) {
        buffer.append(expression.getType().getName());
    }

    @Override
    public void visitClosureExpression(ClosureExpression expression) {
        buffer.append(expression.getText());
    }

    @Override
    public void visitClosureListExpression(ClosureListExpression cle) {
        boolean first = true;
        for (Expression exp : cle.getExpressions()) {
            if (first) {
                first = false;
            } else {
                buffer.append("; ");
            }
            exp.visit(this);
        }
        buffer.append(")");
    }

    @Override
    public void visitConstructorCallExpression(ConstructorCallExpression call) {
        String text = null;
        if (call.isSuperCall()) {
            text = "super ";
        } else if (call.isThisCall()) {
            text = "this ";
        } else {
            text = "new " + call.getType().getName();
        }
        buffer.append(text);
        call.getArguments().visit(this);

    }

    @Override
    public void visitFieldExpression(FieldExpression expression) {
        buffer.append(expression.getText());
    }

    @Override
    public void visitGStringExpression(GStringExpression expression) {
        buffer.append("\"");
        Iterator<ConstantExpression> stringIterator = expression.getStrings().iterator();
        Iterator<Expression> valueIterator = expression.getValues().iterator();
        while (stringIterator.hasNext()) {
            buffer.append(stringIterator.next().getText());
            if (valueIterator.hasNext()) {
                buffer.append("${");
                valueIterator.next().visit(this);
                buffer.append("}");              
            }            
        }
        buffer.append("\"");
            
        
    }

    @Override
    public void visitListExpression(ListExpression expression) {
        buffer.append("[");
        boolean first = true;
        for (Expression ex : expression.getExpressions()) {
            if (first) {
                first = false;
            } else {
                buffer.append(", ");
            }
            ex.visit(this);
        }
        buffer.append("]");
    }

    @Override
    public void visitMapExpression(MapExpression expression) {

        buffer.append("[");
        int size = expression.getMapEntryExpressions().size();
        MapEntryExpression mapEntryExpression = null;
        if (size > 0) {
            mapEntryExpression = expression.getMapEntryExpressions().get(0);
            mapEntryExpression.getKeyExpression().visit(this);
            buffer.append(":");
            mapEntryExpression.getValueExpression().visit(this);
            for (int i = 1; i < size; i++) {
                mapEntryExpression = expression.getMapEntryExpressions().get(i);
                buffer.append(", ");
                mapEntryExpression.getKeyExpression().visit(this);
                buffer.append(":");
                mapEntryExpression.getValueExpression().visit(this);
            }
        } else {
            buffer.append(":");
        }
        buffer.append("]");

    }

    @Override
    public void visitPostfixExpression(PostfixExpression expression) {
        buffer.append("(");
        expression.getExpression().visit(this);
        buffer.append(expression.getOperation().getText());
        buffer.append(")");
    }

    @Override
    public void visitPrefixExpression(PrefixExpression expression) {
        buffer.append("(");
        buffer.append(expression.getOperation().getText());
        expression.getExpression().visit(this);
        buffer.append(")");
    }

    @Override
    public void visitPropertyExpression(PropertyExpression expression) {
        expression.getObjectExpression().visit(this);
        buffer.append(expression.isSpreadSafe() ? "*" : "");
        buffer.append(expression.isSafe() ? "?" : "");
        buffer.append(".");
        expression.getProperty().visit(this);
    }

    @Override
    public void visitRangeExpression(RangeExpression expression) {
        buffer.append("(");
        expression.getFrom().visit(this);
        buffer.append(!expression.isInclusive() ? "..<" : "..");
        expression.getTo().visit(this);
        buffer.append(")");
    }

    @Override
    public void visitTupleExpression(TupleExpression expression) {
        buffer.append("(");
        boolean first = true;
        for (Expression ex : expression.getExpressions()) {
            if (first) {
                first = false;
            } else {
                buffer.append(", ");
            }
            ex.visit(this);
        }
        buffer.append(")");
    }

    @Override
    public void visitSpreadExpression(SpreadExpression expression) {
        buffer.append("*");
        expression.getExpression().visit(this);
    }

    @Override
    public void visitSpreadMapExpression(SpreadMapExpression expression) {
        buffer.append("*:");
        expression.getExpression().visit(this);
    }

    @Override
    public void visitStaticMethodCallExpression(StaticMethodCallExpression call) {
        buffer.append(call.getOwnerType().getName());
        buffer.append(".");
        buffer.append(call.getMethod());
        call.getArguments().visit(this);
    }

    @Override
    public void visitTernaryExpression(TernaryExpression expression) {
        buffer.append("(");
        expression.getBooleanExpression().visit(this);
        buffer.append(") ? ");
        expression.getTrueExpression().visit(this);
        buffer.append(" : ");
        expression.getFalseExpression().visit(this);

    }

    @Override
    public void visitMethodCallExpression(MethodCallExpression call) {
        call.getObjectExpression().visit(this);
        buffer.append(call.isSpreadSafe() ? "*" : "");
        buffer.append(call.isSafe() ? "?" : "");
        buffer.append(".");
        call.getMethod().visit(this);
        call.getArguments().visit(this);

    }

    @Override
    public void visitVariableExpression(VariableExpression expression) {
        variables.add(expression.getName());
        buffer.append(expression.getName());
    }
}

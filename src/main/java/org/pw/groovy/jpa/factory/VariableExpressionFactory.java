package org.pw.groovy.jpa.factory;

import org.codehaus.groovy.ast.expr.VariableExpression;
import org.pw.groovy.jpa.Context;
import org.pw.groovy.jpa.expressions.ParsableExpression;
import org.pw.groovy.jpa.expressions.ParsableVariableExpression;

/**
 *
 * @author Paul Weinhold
 */
public class VariableExpressionFactory extends AbstractExpressionFactory<VariableExpression> {

    @Override
    protected ParsableExpression getParsableExpression(Context context, VariableExpression expression) {
        return new ParsableVariableExpression();
    }
}

package org.pw.groovy.jpa.factory;

import groovy.lang.GroovyRuntimeException;
import java.util.HashMap;
import java.util.Map;
import org.codehaus.groovy.ast.expr.BinaryExpression;
import org.codehaus.groovy.ast.expr.ConstantExpression;
import org.codehaus.groovy.ast.expr.Expression;
import org.codehaus.groovy.ast.expr.MethodCallExpression;
import org.codehaus.groovy.ast.expr.PropertyExpression;
import org.codehaus.groovy.ast.expr.VariableExpression;
import org.pw.groovy.jpa.Context;
import org.pw.groovy.jpa.expressions.Checkable;
import org.pw.groovy.jpa.expressions.ParsableExpression;
import org.pw.groovy.jpa.expressions.checkstrategies.CheckStrategyProvider;

/**
 *
 * @author Paul Weinhold
 */
public abstract class AbstractExpressionFactory<E extends Expression> {

    private final static Map<Class<? extends AbstractExpressionFactory>, AbstractExpressionFactory> instances = new HashMap<>();
    private final static Map<Class<? extends Expression>, Class<? extends AbstractExpressionFactory>> factoryRegistry = new HashMap<>();

    static {
        factoryRegistry.put(BinaryExpression.class, BinaryExpressionFactory.class);
        factoryRegistry.put(ConstantExpression.class, ConstantExpressionFactory.class);
        factoryRegistry.put(MethodCallExpression.class, MethodExpressionFactory.class);
        factoryRegistry.put(PropertyExpression.class, PropertyExpressionFactory.class);
        factoryRegistry.put(VariableExpression.class, VariableExpressionFactory.class);
    }

    public ParsableExpression createParsableExpression(Context context, E expression) {
        ParsableExpression parsableExpression = getParsableExpression(context, expression);
            if (parsableExpression instanceof Checkable) {
                ((Checkable) parsableExpression).setCheckStrategy(CheckStrategyProvider.getCheckStrategy(context, expression));
            }
            parsableExpression.setContext(context);
        return parsableExpression;
    }

    protected abstract ParsableExpression getParsableExpression(Context context, E expression);

    public static <T extends AbstractExpressionFactory> T getFactory(Expression expression) {
        return (T) getFactory(factoryRegistry.get(expression.getClass()));
    }

    public static <T extends AbstractExpressionFactory> T getFactory(Class<T> factoryClass) {
        if (factoryClass == null) {
            throw new GroovyRuntimeException("Factory can not be found");
        }

        AbstractExpressionFactory factory = instances.get(factoryClass);
        if (factory == null) {
            try {
                factory = factoryClass.newInstance();
                instances.put(factoryClass, factory);
            } catch (InstantiationException | IllegalAccessException ex) {
            }
        }
        return (T) factory;
    }
}
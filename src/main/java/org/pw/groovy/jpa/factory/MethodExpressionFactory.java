package org.pw.groovy.jpa.factory;

import groovy.lang.GroovyRuntimeException;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.apache.commons.lang.ClassUtils;
import org.codehaus.groovy.ast.expr.ArgumentListExpression;
import org.codehaus.groovy.ast.expr.MethodCallExpression;
import org.pw.groovy.jpa.Context;
import org.pw.groovy.jpa.TypeResolver;
import org.pw.groovy.jpa.expressions.ParsableExpression;
import org.pw.groovy.jpa.expressions.ParsableMethodExpression;
import org.pw.groovy.jpa.expressions.arithmetic.*;
import org.pw.groovy.jpa.expressions.collections.*;
import org.pw.groovy.jpa.expressions.compare.*;
import org.pw.groovy.jpa.expressions.logical.AndExpression;
import org.pw.groovy.jpa.expressions.logical.OrExpression;
import org.pw.groovy.jpa.expressions.range.ContainsWithinBoundsExpression;
import org.pw.groovy.jpa.expressions.strings.*;

/**
 *
 * @author Paul Weinhold
 */
public class MethodExpressionFactory extends AbstractExpressionFactory<MethodCallExpression> {
    
    private static Map<MethodKey, List<MethodEntry>> methods = new HashMap<>();
    
    static {
        
        registerClass(ContainsWithinBoundsExpression.class);
        
        registerClass(AndExpression.class);
        registerClass(OrExpression.class);
        
        registerClass(EqualsExpression.class);
        registerClass(NotEqualsExpression.class);
        registerClass(GreaterThanEqualExpression.class);
        registerClass(GreaterThanExpression.class);
        registerClass(LessThanEqualExpression.class);
        registerClass(LessThanExpression.class);
        
        registerClass(AdditionExpression.class);
        registerClass(DivisionExpression.class);
        registerClass(MultiplicationExpression.class);
        registerClass(SubtractionExpression.class);
        registerClass(AbsExpression.class);
        registerClass(ModExpression.class);
        registerClass(SqrtExpression.class);
        registerClass(PowerExpression.class);
        
        registerClass(CapitalizeExpression.class);
        registerClass(CharAtExpression.class);
        registerClass(ConcatExpression.class);
        registerClass(ContainsExpression.class);
        registerClass(EndsWithExpression.class);
        registerClass(EqualsIgnoreCaseExpression.class);
        registerClass(GetAtExpression.class);
        registerClass(IndexOfExpression.class);
        registerClass(IndexOfFromExpression.class);
        registerClass(IsEmptyExpression.class);
        registerClass(LengthExpression.class);
        registerClass(LikeExpression.class);
        registerClass(LikeEscapeExpression.class);
        registerClass(PlusExpression.class);
        registerClass(SizeExpression.class);
        registerClass(StartsWithExpression.class);
        registerClass(StartsWithIndexExpression.class);
        registerClass(SubstringExpression.class);
        registerClass(SubstringEndIndexExpression.class);
        registerClass(ToLowerExpression.class);
        registerClass(ToUpperExpression.class);
        registerClass(TrimExpression.class);
        
        registerClass(AvgExpression.class);
        registerClass(AvgClosureExpression.class);
        registerClass(MaxExpression.class);
        registerClass(MaxClosureExpression.class);
        registerClass(MinExpression.class);
        registerClass(MinClosureExpression.class);
        registerClass(SumExpression.class);
        registerClass(SumClosureExpression.class);
        registerClass(IsEmptyCollectionExpression.class);
        registerClass(SizeCollectionExpression.class);
        registerClass(CountExpression.class);
        registerClass(AnyExpression.class);
        registerClass(AnyClosureExpression.class);
        registerClass(EveryExpression.class);
        registerClass(EveryClosureExpression.class);
        
    }
    
    @Override
    protected ParsableExpression getParsableExpression(Context context, MethodCallExpression expression) {
        TypeResolver typeResolver = context.getTypeResolver();
        Class objectType = typeResolver.resolve(expression.getObjectExpression());
        if (typeResolver.isCollection()) {
            objectType = Collection.class;
        }
        MethodSignature signature = new MethodSignature(expression.getMethodAsString(), getParameterCount(expression), objectType);
        return getParsableMethodExpression(signature);
    }
    
    private int getParameterCount(MethodCallExpression expression) {
        int arguments = 0;
        if (expression.getArguments() instanceof ArgumentListExpression) {
            arguments = ((ArgumentListExpression) expression.getArguments()).getExpressions().size();
        } else if (expression.getArguments() != null) {
            arguments = 1;
        }
        return arguments;
    }
    
    public static ParsableMethodExpression getParsableMethodExpression(MethodSignature signature) {
        MethodKey key = new MethodKey(signature.getName(), signature.getParameterCount());
        List<MethodEntry> entries = methods.get(key);
        
        if (entries.size() > 1) {
            Collections.sort(entries, new Comparator<MethodEntry>() {
                @Override
                public int compare(MethodEntry o1, MethodEntry o2) {
                    if (o1.getObjectType().equals(Object.class)) {
                        return 1;
                    } else {
                        return -1;
                    }
                }
            });
        }
        if (entries != null) {
            try {
                for (MethodEntry entry : entries) {
                    if (ClassUtils.isAssignable(signature.getObjectType(), entry.getObjectType(), true)) {
                        return entry.getExpressionClass().newInstance();
                    }
                }
            } catch (InstantiationException | IllegalAccessException ex) {
            }
        }
        throw new GroovyRuntimeException();
    }
    
    private static void registerClass(Class<? extends ParsableMethodExpression> clazz) {
        try {
            ParsableMethodExpression pme = clazz.newInstance();
            MethodKey signature = new MethodKey(pme.getMethodName(), pme.getParameterCount());
            MethodEntry entry = new MethodEntry(pme.getObjectType(), clazz);
            if (!methods.containsKey(signature)) {
                methods.put(signature, new LinkedList<MethodEntry>());
            }
            methods.get(signature).add(entry);
        } catch (InstantiationException | IllegalAccessException ex) {
        }
    }
    
    public static class MethodSignature extends MethodKey {
        
        private Class objectType;
        
        public MethodSignature(String name, int parameterCount, Class objectType) {
            super(name, parameterCount);
            this.objectType = objectType;
        }
        
        public Class getObjectType() {
            return objectType;
        }
    }
    
    private static class MethodKey {
        
        private String name;
        private int parameterCount;
        
        public MethodKey(String name, int parameterCount) {
            this.name = name;
            this.parameterCount = parameterCount;
        }
        
        public String getName() {
            return name;
        }
        
        public int getParameterCount() {
            return parameterCount;
        }
        
        @Override
        public int hashCode() {
            int hash = 3;
            hash = 71 * hash + Objects.hashCode(this.name);
            hash = 71 * hash + this.parameterCount;
            return hash;
        }
        
        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final MethodKey other = (MethodKey) obj;
            if (!Objects.equals(this.name, other.name)) {
                return false;
            }
            if (this.parameterCount != other.parameterCount) {
                return false;
            }
            return true;
        }
    }
    
    private static class MethodEntry {
        
        private Class objectType;
        Class<? extends ParsableMethodExpression> expressionClass;
        
        public MethodEntry(Class objectClass, Class<? extends ParsableMethodExpression> expressionClass) {
            this.objectType = objectClass;
            this.expressionClass = expressionClass;
        }
        
        public Class<? extends ParsableMethodExpression> getExpressionClass() {
            return expressionClass;
        }
        
        public Class getObjectType() {
            return objectType;
        }
    }
}

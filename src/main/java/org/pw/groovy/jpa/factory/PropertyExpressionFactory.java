package org.pw.groovy.jpa.factory;

import org.codehaus.groovy.ast.expr.PropertyExpression;
import org.pw.groovy.jpa.Context;
import org.pw.groovy.jpa.expressions.ParsableExpression;
import org.pw.groovy.jpa.expressions.ParsablePropertyExpression;

/**
 *
 * @author Paul Weinhold
 */
public class PropertyExpressionFactory extends AbstractExpressionFactory<PropertyExpression> {

    @Override
    protected ParsableExpression getParsableExpression(Context context, PropertyExpression expression) {
        return new ParsablePropertyExpression();
    }
}

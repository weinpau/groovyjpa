package org.pw.groovy.jpa.factory;

import org.codehaus.groovy.ast.expr.ConstantExpression;
import org.pw.groovy.jpa.Context;
import org.pw.groovy.jpa.expressions.ParsableConstantExpression;
import org.pw.groovy.jpa.expressions.ParsableExpression;

/**
 *
 * @author Paul Weinhold
 */
public class ConstantExpressionFactory extends AbstractExpressionFactory<ConstantExpression> {

    @Override
    protected ParsableExpression getParsableExpression(Context context, ConstantExpression expression) {
        return new ParsableConstantExpression();
    }
}

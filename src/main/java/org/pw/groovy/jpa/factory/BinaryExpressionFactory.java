package org.pw.groovy.jpa.factory;

import groovy.lang.GroovyRuntimeException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.codehaus.groovy.ast.expr.BinaryExpression;
import org.codehaus.groovy.ast.expr.Expression;
import org.codehaus.groovy.ast.expr.MethodCallExpression;
import org.codehaus.groovy.syntax.Types;
import org.pw.groovy.jpa.Context;
import org.pw.groovy.jpa.expressions.ParsableExpression;

/**
 *
 * @author Paul Weinhold
 */
public class BinaryExpressionFactory extends AbstractExpressionFactory<BinaryExpression> {

    private static final Map<Integer, List<BinaryExpressionWrapper>> wrapper = new HashMap<>();

    static {
        registerWrapper(Types.LOGICAL_AND, new LeftBinaryExpressionWrapper("and"));
        registerWrapper(Types.LOGICAL_OR, new LeftBinaryExpressionWrapper("or"));

        registerWrapper(Types.COMPARE_EQUAL, new LeftBinaryExpressionWrapper("equals"));
        registerWrapper(Types.COMPARE_NOT_EQUAL, new LeftBinaryExpressionWrapper("notEquals"));
        registerWrapper(Types.COMPARE_GREATER_THAN, new LeftBinaryExpressionWrapper("greaterThan"));
        registerWrapper(Types.COMPARE_GREATER_THAN_EQUAL, new LeftBinaryExpressionWrapper("greaterThanEqual"));
        registerWrapper(Types.COMPARE_LESS_THAN, new LeftBinaryExpressionWrapper("lessThan"));
        registerWrapper(Types.COMPARE_LESS_THAN_EQUAL, new LeftBinaryExpressionWrapper("lessThanEqual"));

        registerWrapper(Types.PLUS, new LeftBinaryExpressionWrapper("plus", true));
        registerWrapper(Types.MINUS, new LeftBinaryExpressionWrapper("minus"));
        registerWrapper(Types.MULTIPLY, new LeftBinaryExpressionWrapper("multiply", true));
        registerWrapper(Types.DIVIDE, new LeftBinaryExpressionWrapper("div"));
        registerWrapper(Types.POWER, new LeftBinaryExpressionWrapper("power"));
        registerWrapper(Types.MOD, new LeftBinaryExpressionWrapper("mod"));

        registerWrapper(Types.KEYWORD_IN, new RightBinaryExpressionWrapper("containsWithinBounds"));

    }

    private static List<MethodCallExpression> getMethodCallExpressions(BinaryExpression binaryExpression) {
        List<BinaryExpressionWrapper> bews = wrapper.get(binaryExpression.getOperation().getType());

        if (bews != null) {
            List<MethodCallExpression> callExpressions = new LinkedList<>();
            for (BinaryExpressionWrapper w : bews) {
                MethodCallExpression methodCallExpression = new MethodCallExpression(w.getObjectExpresssion(binaryExpression),
                                                w.getMethodName(),
                                                w.getArgumentExpression(binaryExpression));
                
                callExpressions.add(methodCallExpression);
                if (w.isCommutative()) {
                    callExpressions.add(reverse(methodCallExpression));
                }
            }
            return callExpressions;
        }
        throw new GroovyRuntimeException();
    }
    
    private static MethodCallExpression reverse(MethodCallExpression mce) {
        return new MethodCallExpression(mce.getArguments(), mce.getMethodAsString(), mce.getObjectExpression());
    }

    private static void registerWrapper(int type, BinaryExpressionWrapper bew) {
        List<BinaryExpressionWrapper> bews = wrapper.get(type);
        if (bews == null) {
            bews = new LinkedList<>();
            wrapper.put(type, bews);
        }
        bews.add(bew);
    }

    @Override
    protected ParsableExpression getParsableExpression(Context context, BinaryExpression expression) {
        for (final MethodCallExpression methodCallExpression : getMethodCallExpressions(expression)) {
            try {
                final ParsableExpression parsableExpression = getFactory(MethodExpressionFactory.class).createParsableExpression(context, methodCallExpression);
                return new ParsableExpression<BinaryExpression>() {
                    @Override
                    public Class getResultType() {
                        return parsableExpression.getResultType();
                    }

                    @Override
                    public void parse(BinaryExpression expression, boolean negate, boolean appendCheck) {
                        parsableExpression.parse(methodCallExpression, negate, appendCheck);
                    }
                };

            } catch (Exception excep) {
            }
        }
        throw new GroovyRuntimeException();
    }

    private static abstract class BinaryExpressionWrapper {

        private String methodName;
        private boolean commutative;

        public BinaryExpressionWrapper(String methodName, boolean commutative) {
            this.methodName = methodName;
            this.commutative = commutative;
        }

        abstract Expression getObjectExpresssion(BinaryExpression binaryExpression);

        abstract Expression getArgumentExpression(BinaryExpression binaryExpression);

        public String getMethodName() {
            return methodName;
        }

        public boolean isCommutative() {
            return commutative;
        }
    }

    private static class LeftBinaryExpressionWrapper extends BinaryExpressionWrapper {

        public LeftBinaryExpressionWrapper(String methodName) {
            super(methodName, false);
        }

        public LeftBinaryExpressionWrapper(String methodName, boolean commutative) {
            super(methodName, commutative);
        }

        @Override
        Expression getObjectExpresssion(BinaryExpression binaryExpression) {
            return binaryExpression.getLeftExpression();
        }

        @Override
        Expression getArgumentExpression(BinaryExpression binaryExpression) {
            return binaryExpression.getRightExpression();
        }
    }

    private static class RightBinaryExpressionWrapper extends BinaryExpressionWrapper {

        public RightBinaryExpressionWrapper(String methodName) {
            super(methodName, false);
        }

        public RightBinaryExpressionWrapper(String methodName, boolean commutative) {
            super(methodName, commutative);
        }

        @Override
        Expression getObjectExpresssion(BinaryExpression binaryExpression) {
            return binaryExpression.getRightExpression();
        }

        @Override
        Expression getArgumentExpression(BinaryExpression binaryExpression) {
            return binaryExpression.getLeftExpression();
        }
    }
}

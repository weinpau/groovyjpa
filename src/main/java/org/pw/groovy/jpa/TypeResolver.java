package org.pw.groovy.jpa;

import groovy.lang.GroovyRuntimeException;
import groovy.lang.Range;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import org.codehaus.groovy.ast.CodeVisitorSupport;
import org.codehaus.groovy.ast.expr.BinaryExpression;
import org.codehaus.groovy.ast.expr.ConstantExpression;
import org.codehaus.groovy.ast.expr.Expression;
import org.codehaus.groovy.ast.expr.MethodCallExpression;
import org.codehaus.groovy.ast.expr.PropertyExpression;
import org.codehaus.groovy.ast.expr.RangeExpression;
import org.codehaus.groovy.ast.expr.TupleExpression;
import org.codehaus.groovy.ast.expr.VariableExpression;
import org.pw.groovy.jpa.factory.MethodExpressionFactory;

/**
 *
 * @author Paul Weinhold
 */
public class TypeResolver extends CodeVisitorSupport {

    private Stack<Entry> stack = new Stack<>();
    private List<ResolveItem> resolveItems = new ArrayList<>();
    private Class rootClass, resolveType;
    private boolean hasCollection, isCollection;
    private Map<String, Class> contextTypes;
    private Expression lastExpression;

    public TypeResolver(Map<String, Class> contextTypes) {
        this.contextTypes = contextTypes;
    }

    @Override
    public void visitBinaryExpression(BinaryExpression expression) {
        expression.getLeftExpression().visit(this);
    }

    @Override
    public void visitConstantExpression(ConstantExpression expression) {
        try {
            rootClass = expression.getValue().getClass();
        } catch (Exception e) {
        }
    }

    @Override
    public void visitVariableExpression(VariableExpression expression) {
        rootClass = contextTypes.get(expression.getName());
    }

    @Override
    public void visitMethodCallExpression(MethodCallExpression call) {
        stack.push(new MethodEntry(call));
        call.getObjectExpression().visit(this);
    }

    @Override
    public void visitPropertyExpression(PropertyExpression expression) {
        stack.push(new PropertyEntry(expression));
        expression.getObjectExpression().visit(this);
    }

    @Override
    public void visitRangeExpression(RangeExpression expression) {
        rootClass = Range.class;
    }

    public Class resolve(Expression expression) {
        if (!checkExpression(expression)) {
            return getType();
        }

        init();
        expression.visit(this);
        ResultType type = new ResultType(rootClass, false);
        boolean collection = false;
        ResolveItem resolveItem = new ResolveItem();
        resolveItems.add(resolveItem);
        while (!stack.empty()) {
            Entry entry = stack.pop();
            try {
                type = entry.getResultType(collection, type.getType());
                if (entry instanceof MethodEntry) {
                    if (type.isCollection()) {
                        throw new GroovyRuntimeException("GroovyDAO unable to evaluate expression.");
                    } else {
                        collection = false;
                    }
                } else {
                    resolveItem.addProperty(entry.getName());
                    if (type.isCollection()) {
                        collection = true;
                        hasCollection = true;
                        resolveItem.setCollectionType(type.getType());
                        resolveItem = new ResolveItem();
                        resolveItems.add(resolveItem);
                    }
                }
            } catch (Exception ex) {
                isCollection = collection;
                resolveType = Object.class;
                return resolveType;
            }
        }
        isCollection = collection;
        resolveType = type.getType();
        return resolveType;
    }

    public Class getType() {
        return resolveType;
    }

    public boolean hasCollection() {
        return hasCollection;
    }

    public boolean isCollection() {
        return isCollection;
    }

    public List<ResolveItem> getResolveItems() {
        return resolveItems;
    }

    private void init() {
        stack.clear();
        resolveItems.clear();
        resolveType = null;
        hasCollection = false;
        isCollection = false;
    }

    private boolean checkExpression(Expression currentExpression) {
        if (currentExpression == lastExpression) {
            return false;
        }
        lastExpression = currentExpression;
        return true;
    }

    public class ResolveItem {

        private String propertyExpression = "";
        private Class collectionType;

        public ResolveItem() {
        }

        public String getPropertyExpression() {
            return propertyExpression;
        }

        public void setPropertyExpression(String propertyExpression) {
            this.propertyExpression = propertyExpression;
        }

        public Class getCollectionType() {
            return collectionType;
        }

        public void setCollectionType(Class collectionType) {
            this.collectionType = collectionType;
        }

        public boolean hasCollection() {
            return collectionType != null;
        }

        public void addProperty(String property) {
            if (!propertyExpression.isEmpty()) {
                propertyExpression += ".";
            }
            propertyExpression += property;

        }
    }

    private abstract class Entry<E extends Expression> {

        protected E expression;

        public Entry(E expression) {
            this.expression = expression;
        }

        public abstract String getName();

        public abstract ResultType getResultType(boolean collection, Class targetClass);
    }

    private class PropertyEntry extends Entry<PropertyExpression> {

        public PropertyEntry(PropertyExpression expression) {
            super(expression);
        }

        @Override
        public String getName() {
            return expression.getPropertyAsString();
        }

        @Override
        public ResultType getResultType(boolean collection, Class targetClass) {
            try {
                Field field = targetClass.getDeclaredField(getName());
                Class type = field.getType();
                if (Collection.class.isAssignableFrom(type)) {
                    type = resolveGenericFieldType(field, type);
                    return new ResultType(type, true);
                } else {
                    return new ResultType(type, false);
                }
            } catch (NoSuchFieldException | SecurityException exception) {
                return null;
            }
        }

        private Class resolveGenericType(Type genericType) {
            try {
                ParameterizedType gi = (ParameterizedType) genericType;
                return (Class) gi.getActualTypeArguments()[0];
            } catch (Exception e) {
                return null;
            }
        }

        private Class resolveGenericFieldType(Field field, Class type) {
            Class gType = resolveGenericType(field.getGenericType());
            if (gType != null) {
                type = gType;
            }
            return type;
        }
    }

    private class MethodEntry extends Entry<MethodCallExpression> {

        public MethodEntry(MethodCallExpression expression) {
            super(expression);
        }

        @Override
        public String getName() {
            return expression.getMethodAsString();
        }

        private int getParameterCount() {
            return ((TupleExpression) expression.getArguments()).getExpressions().size();
        }

        @Override
        public ResultType getResultType(boolean hasCollection, Class targetClass) {
            if (hasCollection) {
                targetClass = Collection.class;
            }
            MethodExpressionFactory.MethodSignature signature = new MethodExpressionFactory.MethodSignature(getName(), getParameterCount(), targetClass);
            return new ResultType(MethodExpressionFactory.getParsableMethodExpression(signature).getResultType(), false);
        }
    }

    private class ResultType {

        private Class type;
        private boolean collection;

        public ResultType(Class type, boolean collection) {
            this.type = type;
            this.collection = collection;
        }

        public boolean isCollection() {
            return collection;
        }

        public Class getType() {
            return type;
        }
    }
}

package org.pw.groovy.jpa.visitors;

import java.util.Iterator;
import org.codehaus.groovy.ast.Parameter;
import org.codehaus.groovy.ast.Variable;
import org.codehaus.groovy.ast.expr.BinaryExpression;
import org.codehaus.groovy.ast.expr.BooleanExpression;
import org.codehaus.groovy.ast.expr.ClassExpression;
import org.codehaus.groovy.ast.expr.ConstantExpression;
import org.codehaus.groovy.ast.expr.Expression;
import org.codehaus.groovy.ast.expr.ListExpression;
import org.codehaus.groovy.ast.expr.MethodCallExpression;
import org.codehaus.groovy.ast.expr.NotExpression;
import org.codehaus.groovy.ast.expr.PropertyExpression;
import org.codehaus.groovy.ast.expr.VariableExpression;
import org.codehaus.groovy.ast.stmt.BlockStatement;
import org.codehaus.groovy.ast.stmt.ReturnStatement;
import org.pw.groovy.jpa.Context;
import org.pw.groovy.jpa.expressions.ParsableExpression;
import org.pw.groovy.jpa.factory.AbstractExpressionFactory;

/**
 *
 * @author Paul Weinhold
 */
public class WhereVisitor extends AbstractVisitor {

    protected Context context;
    private boolean appendCheck, negate;

    public WhereVisitor(Context context, boolean negate, boolean appendcheck) {
        this.context = context;
        this.appendCheck = appendcheck;
        this.negate = negate;
    }

    @Override
    public void visitBlockStatement(BlockStatement block) {
        Iterator<Variable> iterator = block.getVariableScope().getReferencedLocalVariablesIterator();
        while (iterator.hasNext()) {
            Variable var = iterator.next();
            if (var instanceof Parameter) {
                context.setVariable(var.getName());
                break;
            }
        }
        super.visitBlockStatement(block);
    }

    @Override
    public void visitBinaryExpression(BinaryExpression expression) {
        boolean leaf = !(expression.getRightExpression() instanceof BinaryExpression)
                && !(expression.getLeftExpression() instanceof BinaryExpression);
        if (!leaf) {
            context.append("(");
        }
        parseExpression(expression);
        if (!leaf) {
            context.append(")");
        }
    }

    @Override
    public void visitBooleanExpression(BooleanExpression expression) {
        expression.getExpression().visit(this);
    }

    @Override
    public void visitClassExpression(ClassExpression expression) {
        context.append(expression.getText());
    }

    @Override
    public void visitConstantExpression(ConstantExpression expression) {
        parseExpression(expression);
    }

    @Override
    public void visitListExpression(ListExpression expression) {
        context.append("(");
        Iterator<Expression> iterator = expression.getExpressions().iterator();
        while (iterator.hasNext()) {
            context.parse(iterator.next(), false, false);
            if (iterator.hasNext()) {
                context.append(", ");
            }
        }
        context.append(")");
    }

    @Override
    public void visitMethodCallExpression(MethodCallExpression call) {
        parseExpression(call);
    }

    @Override
    public void visitNotExpression(NotExpression expression) {
        expression.getExpression().visit(new WhereVisitor(context, !negate, appendCheck));
    }

    @Override
    public void visitPropertyExpression(PropertyExpression expression) {
        parseExpression(expression);
    }

    @Override
    public void visitReturnStatement(ReturnStatement statement) {
        statement.getExpression().visit(this);
    }

    @Override
    public void visitVariableExpression(VariableExpression expression) {
        parseExpression(expression);
    }

    private void parseExpression(Expression expression) {
        parseExpression(expression, appendCheck);
    }

    protected void parseExpression(Expression expression, boolean appendCheck) {
        ParsableExpression parsableExpression = getParsableExpression(expression);
        parsableExpression.parse(expression, negate, appendCheck);
    }

    protected ParsableExpression getParsableExpression(Expression expression) {
        AbstractExpressionFactory factory = AbstractExpressionFactory.getFactory(expression);
        return factory.createParsableExpression(context, expression);
    }
}

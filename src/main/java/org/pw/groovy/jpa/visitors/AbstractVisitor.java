package org.pw.groovy.jpa.visitors;

import groovy.lang.GroovyRuntimeException;
import org.codehaus.groovy.ast.CodeVisitorSupport;
import org.codehaus.groovy.ast.expr.AttributeExpression;
import org.codehaus.groovy.ast.expr.CastExpression;
import org.codehaus.groovy.ast.expr.ConstructorCallExpression;
import org.codehaus.groovy.ast.expr.DeclarationExpression;
import org.codehaus.groovy.ast.expr.ElvisOperatorExpression;
import org.codehaus.groovy.ast.expr.Expression;
import org.codehaus.groovy.ast.expr.GStringExpression;
import org.codehaus.groovy.ast.expr.MethodPointerExpression;
import org.codehaus.groovy.ast.expr.StaticMethodCallExpression;
import org.codehaus.groovy.ast.expr.TernaryExpression;
import org.codehaus.groovy.ast.stmt.AssertStatement;
import org.codehaus.groovy.ast.stmt.BreakStatement;
import org.codehaus.groovy.ast.stmt.CaseStatement;
import org.codehaus.groovy.ast.stmt.CatchStatement;
import org.codehaus.groovy.ast.stmt.ContinueStatement;
import org.codehaus.groovy.ast.stmt.DoWhileStatement;
import org.codehaus.groovy.ast.stmt.ForStatement;
import org.codehaus.groovy.ast.stmt.IfStatement;
import org.codehaus.groovy.ast.stmt.Statement;
import org.codehaus.groovy.ast.stmt.SwitchStatement;
import org.codehaus.groovy.ast.stmt.SynchronizedStatement;
import org.codehaus.groovy.ast.stmt.ThrowStatement;
import org.codehaus.groovy.ast.stmt.TryCatchStatement;
import org.codehaus.groovy.ast.stmt.WhileStatement;

/**
 *
 * @author Paul Weinhold
 */
public abstract class AbstractVisitor extends CodeVisitorSupport {
    
    private static final String NOT_ALLOWED = " is not allowed in this context";
    
    @Override
    public void visitAssertStatement(AssertStatement statement) {
        notAllowed(statement);
    }
    
    @Override
    public void visitAttributeExpression(AttributeExpression expression) {
        notAllowed(expression);
    }
    
    @Override
    public void visitBreakStatement(BreakStatement statement) {
        notAllowed(statement);
    }
    
    @Override
    public void visitCaseStatement(CaseStatement statement) {
        notAllowed(statement);
    }
    
    @Override
    public void visitCatchStatement(CatchStatement statement) {
        notAllowed(statement);
    }
    
    @Override
    public void visitContinueStatement(ContinueStatement statement) {
        notAllowed(statement);
    }
    
    @Override
    public void visitDoWhileLoop(DoWhileStatement loop) {
        notAllowed(loop);
    }
    
    @Override
    public void visitShortTernaryExpression(ElvisOperatorExpression expression) {
        notAllowed(expression);
    }
    
    @Override
    public void visitMethodPointerExpression(MethodPointerExpression expression) {
        notAllowed(expression);
    }
    
    @Override
    public void visitForLoop(ForStatement forLoop) {
        notAllowed(forLoop);
    }
    
    @Override
    public void visitGStringExpression(GStringExpression expression) {
        notAllowed(expression);
    }
    
    @Override
    public void visitIfElse(IfStatement ifElse) {
        notAllowed(ifElse);
    }
    
    @Override
    public void visitStaticMethodCallExpression(StaticMethodCallExpression call) {
        notAllowed(call);
    }
    
    @Override
    public void visitWhileLoop(WhileStatement loop) {
        notAllowed(loop);
    }
    
    @Override
    public void visitTryCatchFinally(TryCatchStatement statement) {
        notAllowed(statement);
    }
    
    @Override
    public void visitSwitch(SwitchStatement statement) {
        notAllowed(statement);
    }
    
    @Override
    public void visitDeclarationExpression(DeclarationExpression expression) {
        notAllowed(expression);
    }
    
    @Override
    public void visitCastExpression(CastExpression expression) {
        notAllowed(expression);
    }
    
    @Override
    public void visitTernaryExpression(TernaryExpression expression) {
        notAllowed(expression);
    }
    
    @Override
    public void visitThrowStatement(ThrowStatement statement) {
        notAllowed(statement);
    }
    
    @Override
    public void visitConstructorCallExpression(ConstructorCallExpression call) {
        notAllowed(call);
    }
    
    @Override
    public void visitSynchronizedStatement(SynchronizedStatement statement) {
        notAllowed(statement);
    }
    
    protected void notAllowed(Statement s) {
        throw new GroovyRuntimeException(s.getText() + NOT_ALLOWED);
    }
    
    protected void notAllowed(Expression e) {
        throw new GroovyRuntimeException(e.getText() + NOT_ALLOWED);
    }
}

package org.pw.groovy.jpa.visitors;

import groovy.lang.GroovyRuntimeException;
import org.codehaus.groovy.ast.expr.ConstructorCallExpression;
import org.codehaus.groovy.ast.expr.Expression;
import org.codehaus.groovy.ast.expr.TupleExpression;
import org.pw.groovy.jpa.Context;
import org.pw.groovy.jpa.expressions.ParsableExpression;

/**
 *
 * @author Paul Weinhold
 */
public class SelectItemVisitor extends WhereVisitor {

    public SelectItemVisitor(Context context) {
        super(context, false, false);
    }

    @Override
    public void visitTupleExpression(TupleExpression expression) {
        context.append("(");
        boolean first = true;
        for (Expression ex : expression.getExpressions()) {
            if (first) {
                first = false;
            } else {
                context.append(", ");
            }
            ex.visit(this);
        }
        context.append(")");
    }

    @Override
    public void visitConstructorCallExpression(ConstructorCallExpression call) {
        context.append("new ");
        context.append(call.getType().getName());
        call.getArguments().visit(this);
    }

  
    @Override
    protected void parseExpression(Expression expression, boolean appendCheck) {
        ParsableExpression parsableExpression = getParsableExpression(expression);
        if (parsableExpression.getResultType().equals(Boolean.class)) {
            throw new GroovyRuntimeException("The " + expression.getText() + " expression is not allowed in this context.");
        }
        parsableExpression.parse(expression, false, appendCheck);
    }
}

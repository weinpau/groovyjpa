package org.pw.groovy.jpa.visitors;

import java.util.Iterator;
import org.codehaus.groovy.ast.Parameter;
import org.codehaus.groovy.ast.Variable;
import org.codehaus.groovy.ast.expr.BinaryExpression;
import org.codehaus.groovy.ast.expr.ConstantExpression;
import org.codehaus.groovy.ast.expr.MethodCallExpression;
import org.codehaus.groovy.ast.expr.PropertyExpression;
import org.codehaus.groovy.ast.expr.VariableExpression;
import org.codehaus.groovy.ast.stmt.BlockStatement;
import org.codehaus.groovy.ast.stmt.ReturnStatement;

/**
 *
 * @author Paul Weinhold
 */
public class SinglePropertyVisitor extends AbstractVisitor {

    protected StringBuffer buffer = new StringBuffer();
    private String closureParameter, identificationVariable;

    public SinglePropertyVisitor(String identificationVariable) {
        this.identificationVariable = identificationVariable;
    }

    public String getClause() {
        return buffer.toString();
    }

    @Override
    public void visitBlockStatement(BlockStatement block) {
        Iterator<Variable> iterator = block.getVariableScope().getReferencedLocalVariablesIterator();
        while (iterator.hasNext()) {
            Variable var = iterator.next();
            if (var instanceof Parameter) {
                closureParameter = var.getName();
                break;
            }
        }
        super.visitBlockStatement(block);
    }

    @Override
    public void visitReturnStatement(ReturnStatement statement) {
        statement.getExpression().visit(this);
    }

    @Override
    public void visitPropertyExpression(PropertyExpression expression) {
        String text = expression.getText();
        if (text.startsWith(getClosureParameter())) {
            text = text.replaceFirst(getClosureParameter(), getIdentificationVariable());
        }
        buffer.append(text);
    }

    @Override
    public void visitBinaryExpression(BinaryExpression expression) {
        notAllowed(expression);
    }

    @Override
    public void visitMethodCallExpression(MethodCallExpression call) {
        notAllowed(call);
    }

    @Override
    public void visitConstantExpression(ConstantExpression expression) {
        notAllowed(expression);
    }

    public String getClosureParameter() {
        return closureParameter;
    }

    public String getIdentificationVariable() {
        return identificationVariable;
    }
}

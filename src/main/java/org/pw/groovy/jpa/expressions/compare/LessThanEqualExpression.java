package org.pw.groovy.jpa.expressions.compare;

import org.codehaus.groovy.ast.expr.MethodCallExpression;
import org.codehaus.groovy.syntax.Types;

/**
 *
 * @author Paul Weinhold
 */
public class LessThanEqualExpression extends CompareExpression {

    @Override
    protected String getOperator() {
        return "<=";
    }

    @Override
    public String getMethodName() {
        return "lessThanEqual";
    }

    @Override
    protected void nullCheck(MethodCallExpression methodCallExpression) {
        getContext().appendNullCheck(methodCallExpression.getObjectExpression(), false);
    }
}

package org.pw.groovy.jpa.expressions;

import org.codehaus.groovy.ast.expr.Expression;
import org.pw.groovy.jpa.Context;

/**
 *
 * @author Paul Weinhold
 */
public abstract class ParsableExpression<E extends Expression> implements Parsable<E> {

    private Context context;

    public void setContext(Context context) {
        this.context = context;
    }

    public Context getContext() {
        return context;
    }

    public abstract Class getResultType();

    public boolean isNullCheckable() {
        return true;
    }
}

package org.pw.groovy.jpa.expressions;

import org.codehaus.groovy.ast.expr.MethodCallExpression;
import org.pw.groovy.jpa.expressions.checkstrategies.CheckStrategy;

/**
 *
 * @author Paul Weinhold
 */
public abstract class CheckableMethodExpression extends ParsableMethodExpression implements Checkable<MethodCallExpression> {

    private CheckStrategy<MethodCallExpression> checkStrategy;
    
    @Override
    public CheckStrategy<MethodCallExpression> getCheckStrategy() {
      return checkStrategy;
    }

    @Override
    public void setCheckStrategy(CheckStrategy<MethodCallExpression> checkStrategy) {
     this.checkStrategy = checkStrategy;
    }   
}

package org.pw.groovy.jpa.expressions.checkstrategies;

import org.codehaus.groovy.ast.expr.Expression;
import org.pw.groovy.jpa.Context;

/**
 *
 * @author Paul Weinhold
 */
public class ObjectCheck<E extends Expression> implements CheckStrategy<E> {

    @Override
    public void appendCheck(Context context, E expression, boolean negate) {
        if (negate) {
            context.append(" IS NULL");
        } else {
            context.append(" IS NOT NULL");
        }
    }
}

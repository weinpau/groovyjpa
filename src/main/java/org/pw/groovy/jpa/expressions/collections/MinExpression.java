package org.pw.groovy.jpa.expressions.collections;

/**
 *
 * @author Paul Weinhold
 */
public class MinExpression extends AggregateExpression {

    @Override
    public Class getResultType() {
        return Object.class;
    }  
    
    @Override
    public String getMethodName() {
        return "min";
    }
}

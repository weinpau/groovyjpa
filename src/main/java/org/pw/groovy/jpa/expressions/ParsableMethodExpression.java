package org.pw.groovy.jpa.expressions;

import org.codehaus.groovy.ast.expr.Expression;
import org.codehaus.groovy.ast.expr.MethodCallExpression;

/**
 *
 * @author Paul Weinhold
 */
public abstract class ParsableMethodExpression extends ParsableExpression<MethodCallExpression> {

    public abstract String getMethodName();

    public abstract int getParameterCount();

    public abstract Class getObjectType();

 }

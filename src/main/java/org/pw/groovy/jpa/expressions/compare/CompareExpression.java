package org.pw.groovy.jpa.expressions.compare;

import org.codehaus.groovy.ast.expr.MethodCallExpression;
import org.pw.groovy.jpa.Context;
import org.pw.groovy.jpa.expressions.ParsableMethodExpression;

/**
 *
 * @author Paul Weinhold
 */
public abstract class CompareExpression extends ParsableMethodExpression {

    @Override
    public Class getObjectType() {
        return Object.class;
    }

    @Override
    public Class getResultType() {
        return Boolean.class;
    }

    @Override
    public int getParameterCount() {
        return 1;
    }

    protected boolean notObjectNull() {
        return false;
    }

    protected boolean notArgumentNull() {
        return false;
    }

    protected void nullCheck(MethodCallExpression methodCallExpression) {
        getContext().append("(");
        getContext().appendNullCheck(methodCallExpression.getObjectExpression(), notObjectNull());
        getContext().append(" AND ");
        getContext().appendNullCheck(methodCallExpression.getArguments(), notArgumentNull());
        getContext().append(")");
    }

    protected abstract String getOperator();

    @Override
    public void parse(MethodCallExpression expression, boolean negate, boolean appendCheck) {
        Context c = getContext();
        if (negate) {
            c.append("(NOT ");
        }
        c.append("(");
        nullCheck(expression);
        if (!c.isNullValue(expression.getObjectExpression())
                && !c.isNullValue(expression.getArguments())) {
            c.append(" OR (");
            c.parse(expression.getObjectExpression(), false, false);
            c.append(getOperator());
            c.append(" ");
            c.parse(expression.getArguments(), false, false);
            c.append(" AND (");
            c.appendNullCheck(expression.getObjectExpression(), true);
            c.append(" AND ");
            c.appendNullCheck(expression.getArguments(), true);
            c.append("))");
        }
        c.append(")");
        if (negate) {
            c.append(")");
        }
    }   
}

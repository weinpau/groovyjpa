package org.pw.groovy.jpa.expressions.collections;

/**
 *
 * @author Paul Weinhold
 */
public class AvgClosureExpression extends AvgExpression {

    @Override
    public int getParameterCount() {
        return 1;
    }
}

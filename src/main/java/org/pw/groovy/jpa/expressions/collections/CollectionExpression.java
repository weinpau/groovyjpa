package org.pw.groovy.jpa.expressions.collections;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import org.codehaus.groovy.ast.expr.ArgumentListExpression;
import org.codehaus.groovy.ast.expr.ClosureExpression;
import org.codehaus.groovy.ast.expr.Expression;
import org.codehaus.groovy.ast.expr.MethodCallExpression;
import org.pw.groovy.jpa.Context;
import org.pw.groovy.jpa.IdentificationVariableCreator;
import org.pw.groovy.jpa.TypeResolver;
import org.pw.groovy.jpa.TypeResolver.ResolveItem;
import org.pw.groovy.jpa.expressions.Checkable;
import org.pw.groovy.jpa.expressions.ParsableMethodExpression;
import org.pw.groovy.jpa.expressions.checkstrategies.CheckStrategy;

/**
 *
 * @author Paul Weinhold
 */
public abstract class CollectionExpression extends ParsableMethodExpression implements Checkable<MethodCallExpression> {

    private CheckStrategy<MethodCallExpression> checkStrategy;

    @Override
    public Class getObjectType() {
        return Collection.class;
    }

    protected void appendSubquery(MethodCallExpression expression, String iv, String aggregate, Where where) {
        appendSubquery(getTypeResolver(expression), iv, aggregate, where);
    }

    protected void appendSubquery(TypeResolver typeResolver, String iv, String aggregate, Where where) {
        Context c = getContext();
        c.append("(SELECT ");
        if (aggregate != null) {
            c.append(aggregate);
            c.append("(");
            c.append(getSelectParameter(iv, typeResolver));
            c.append(")");
        } else {
            c.append(getSelectParameter(iv, typeResolver));
        }
        List<ResolveItem> resolveItems = typeResolver.getResolveItems();

        c.append(" FROM ");

        for (int i = resolveItems.size() - 1; i >= 0; i--) {
            if (resolveItems.get(i).hasCollection()) {
                c.appendEntity(resolveItems.get(i).getCollectionType());
                break;
            }
        }
        c.append(" ");
        c.append(iv);

        String lastCollection = "";
        String joinRootIV = c.getIdentificationVariable();
        List<String> joins = new LinkedList<>();
        for (ResolveItem rItem : resolveItems) {
            if (rItem.hasCollection()) {
                String ivjoin = IdentificationVariableCreator.createIdentificationVariable();
                lastCollection = joinRootIV + "." + rItem.getPropertyExpression();
                joins.add(lastCollection + " " + ivjoin);
                joinRootIV = ivjoin;
            }
        }

        Iterator<String> iterator = joins.iterator();
        while (iterator.hasNext()) {
            String join = iterator.next();
            if (iterator.hasNext()) {
                appendJoin(join);
            }
        }

        c.append(" WHERE ");
        c.append(iv);
        c.append(" MEMBER OF ");
        c.append(lastCollection);

        if (where != null) {
            c.append(" AND (");
            where.appendWhere();
            c.append(")");
        }

        c.append(")");
    }

    protected void appendClosureSubquery(MethodCallExpression expression, String iv, Expression closureArgument, String function) {
        appendClosureSubquery(getTypeResolver(expression), iv, closureArgument, function);
    }

    protected void appendClosureSubquery(final TypeResolver typeResolver, final String iv, Expression closureArgument, String function) {
        final Context c = new Context(getContext(), typeResolver.getType(), getSelectParameter(iv, typeResolver));
        c.parse(closureArgument, false, true);
        Where where = null;
        if (!c.getClause().isEmpty()) {
            where = new Where() {
                @Override
                public void appendWhere() {
                    getContext().append(c.getClause());
                }
            };
        }
        appendSubquery(typeResolver, iv, function, where);
        getContext().appendJoin(c.getJoins());

    }

    protected void appendAggregateFunction(MethodCallExpression expression, String function) {
        TypeResolver typeResolver = getTypeResolver(expression);
        String iv = IdentificationVariableCreator.createIdentificationVariable();
        ArgumentListExpression arguments = (ArgumentListExpression) expression.getArguments();
        if (arguments.getExpressions().size() == 1 && arguments.getExpression(0) instanceof ClosureExpression) {
            final Context c = new Context(getContext(), typeResolver.getType(), getSelectParameter(iv, typeResolver));
            c.parse(arguments.getExpression(0), false, true);
            appendSubquery(typeResolver, iv, function, new Where() {
                @Override
                public void appendWhere() {
                    c.append(c.getClause());
                }
            });
        } else {
            appendSubquery(typeResolver, iv, function, null);
        }
    }

    protected String getSelectParameter(String iv, TypeResolver typeResolver) {
        String selectParameter = iv;
        ResolveItem resolveItem = typeResolver.getResolveItems().get(typeResolver.getResolveItems().size() - 1);
        if (!resolveItem.getPropertyExpression().isEmpty()) {
            selectParameter += "." + resolveItem.getPropertyExpression();
        }
        return selectParameter;
    }

    private void appendJoin(String join) {
        getContext().appendJoin(" JOIN " + join);
    }

    protected TypeResolver getTypeResolver(MethodCallExpression expression) {
        TypeResolver typeResolver = getContext().getTypeResolver();
        typeResolver.resolve(expression.getObjectExpression());
        return typeResolver;
    }

    @Override
    public CheckStrategy<MethodCallExpression> getCheckStrategy() {
        return checkStrategy;
    }

    @Override
    public void setCheckStrategy(CheckStrategy<MethodCallExpression> checkStrategy) {
        this.checkStrategy = checkStrategy;
    }

    protected interface Where {

        void appendWhere();
    }
}

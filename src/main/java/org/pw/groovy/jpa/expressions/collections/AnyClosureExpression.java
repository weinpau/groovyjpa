package org.pw.groovy.jpa.expressions.collections;

import org.codehaus.groovy.ast.expr.MethodCallExpression;
import org.pw.groovy.jpa.Context;
import org.pw.groovy.jpa.IdentificationVariableCreator;

/**
 *
 * @author Paul Weinhold
 */
public class AnyClosureExpression extends AnyExpression {

    @Override
    public int getParameterCount() {
        return 1;
    }

    @Override
    public void parse(MethodCallExpression expression, boolean negate, boolean appendCheck) {
        Context c = getContext(); 
        if (negate) {
            c.append("NOT ");
        }
        c.append("(EXISTS ");
        appendClosureSubquery(getTypeResolver(expression),
                IdentificationVariableCreator.createIdentificationVariable(),
                expression.getArguments(), null);
     
        c.append(")");
    }
}

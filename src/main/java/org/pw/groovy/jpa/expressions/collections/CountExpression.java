package org.pw.groovy.jpa.expressions.collections;

import org.codehaus.groovy.ast.expr.ArgumentListExpression;
import org.codehaus.groovy.ast.expr.ClosureExpression;
import org.codehaus.groovy.ast.expr.Expression;
import org.codehaus.groovy.ast.expr.MethodCallExpression;
import org.pw.groovy.jpa.Context;
import org.pw.groovy.jpa.IdentificationVariableCreator;
import org.pw.groovy.jpa.TypeResolver;

/**
 *
 * @author Paul Weinhold
 */
public class CountExpression extends CollectionExpression {

    @Override
    public String getMethodName() {
        return "count";
    }

    @Override
    public int getParameterCount() {
        return 1;
    }

    @Override
    public Class getResultType() {
        return Long.class;
    }

    @Override
    public void parse(final MethodCallExpression expression, final boolean negate, boolean appendCheck) {
        final String iv = IdentificationVariableCreator.createIdentificationVariable();
        Expression argument = ((ArgumentListExpression) expression.getArguments()).getExpression(0);
        if (argument instanceof ClosureExpression) {
            appendClosureSubquery(expression, iv, argument, "COUNT");
        } else {
            final TypeResolver typeResolver = getTypeResolver(expression);
            final Context c = getContext();
            appendSubquery(typeResolver, iv, "COUNT", new Where() {
                @Override
                public void appendWhere() {
                    c.append(getSelectParameter(iv, typeResolver));
                    c.append(negate ? " <> " : " = ");
                    c.parse(expression.getArguments(), false, false);
                }
            });
        }
    }
}

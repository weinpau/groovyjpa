package org.pw.groovy.jpa.expressions.arithmetic;

import org.codehaus.groovy.ast.expr.MethodCallExpression;
import org.pw.groovy.jpa.Context;

/**
 *
 * @author Paul Weinhold
 */
public class ModExpression extends ArithmeticExpression {

    @Override
    public String getMethodName() {
        return "mod";
    }

    @Override
    public void parse(MethodCallExpression expression, boolean negate, boolean appendCheck) {
        Context c = getContext();
        c.append("MOD (");
        c.parse(expression.getObjectExpression(), false, false);
        c.append(", ");
        c.parse(expression.getArguments(), false, false);
        c.append(")");

        if (appendCheck) {
            getCheckStrategy().appendCheck(c, expression, negate);
        }
    }
}

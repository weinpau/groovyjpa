package org.pw.groovy.jpa.expressions.logical;

/**
 *
 * @author Paul Weinhold
 */
public class AndExpression extends LogicalExpression {

    @Override
    public String getMethodName() {
        return "and";
    }

    @Override
    public String getOperator(boolean negate) {
        return negate ? "OR" : "AND";
    }
}

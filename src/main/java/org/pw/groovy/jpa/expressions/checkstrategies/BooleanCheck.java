package org.pw.groovy.jpa.expressions.checkstrategies;

import org.codehaus.groovy.ast.expr.Expression;
import org.pw.groovy.jpa.Context;

/**
 *
 * @author Paul Weinhold
 */
public class BooleanCheck<E extends Expression> implements CheckStrategy<E> {

    @Override
    public void appendCheck(Context context, E expression, boolean negate) {
        context.append(" = ");
        context.appendParameter(!negate);
    }
}

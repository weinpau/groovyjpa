package org.pw.groovy.jpa.expressions.strings;

import org.codehaus.groovy.ast.expr.ArgumentListExpression;
import org.codehaus.groovy.ast.expr.MethodCallExpression;
import org.pw.groovy.jpa.Context;

/**
 *
 * @author Paul Weinhold
 */
public class LikeEscapeExpression extends LikeExpression {

    @Override
    public int getParameterCount() {
        return 2;
    }

    @Override
    public void parse(MethodCallExpression expression, boolean negate, boolean appendCheck) {
        Context c = getContext();
        ArgumentListExpression ale = (ArgumentListExpression) expression.getArguments();
        if (negate) {
            c.append("(");
        }
        c.parse(expression.getObjectExpression(), false, false);
        c.append(" LIKE ");
        c.parse(ale.getExpression(0), false, false);
        c.append(" ESCAPE ");
        c.parse(ale.getExpression(1), false, false);
        if (negate) {
            c.append(") = FALSE");
        }
    }
}

package org.pw.groovy.jpa.expressions.compare;

/**
 *
 * @author Paul Weinhold
 */
public class GreaterThanExpression extends CompareExpression {

    @Override
    protected String getOperator() {
        return ">";
    }

    @Override
    public String getMethodName() {
        return "greaterThan";
    }

    @Override
    protected boolean notObjectNull() {
        return true;
    }
}

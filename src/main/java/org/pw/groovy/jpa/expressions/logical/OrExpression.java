package org.pw.groovy.jpa.expressions.logical;

/**
 *
 * @author Paul Weinhold
 */
public class OrExpression extends LogicalExpression {

    @Override
    public String getMethodName() {
        return "or";
    }

    @Override
    public String getOperator(boolean negate) {
        return negate ? "AND" : "OR";
    }
}

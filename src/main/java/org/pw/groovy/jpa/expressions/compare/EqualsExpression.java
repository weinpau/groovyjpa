package org.pw.groovy.jpa.expressions.compare;

import org.apache.commons.lang.ClassUtils;
import org.codehaus.groovy.ast.expr.Expression;
import org.codehaus.groovy.ast.expr.MethodCallExpression;
import org.codehaus.groovy.ast.expr.NotExpression;
import org.codehaus.groovy.ast.expr.PropertyExpression;
import org.codehaus.groovy.ast.expr.TupleExpression;
import org.pw.groovy.jpa.CodeVisitor;
import org.pw.groovy.jpa.Context;
import org.pw.groovy.jpa.expressions.ParsableExpression;
import org.pw.groovy.jpa.factory.AbstractExpressionFactory;

/**
 *
 * @author Paul Weinhold
 */
public class EqualsExpression extends CompareExpression {

    @Override
    public String getMethodName() {
        return "equals";
    }

    @Override
    public void parse(MethodCallExpression expression, boolean negate, boolean appendCheck) {
        if (containsBooleanValues(expression, negate)) {
            return;
        }
        super.parse(expression, negate, appendCheck);
    }

    private Boolean getBooleanValue(Expression expression) {
        CodeVisitor codeVisitor = new CodeVisitor();
        expression.visit(codeVisitor);
        if (!getContext().hasClosureParameter(codeVisitor.getVariables())) {
            Object value = getContext().getClosureContext().evaluate(codeVisitor.getCode());
            if (value != null
                    && ClassUtils.isAssignable(value.getClass(), Boolean.class, true)) {
                return (Boolean) value;
            }
        }
        return null;
    }

    private boolean containsBooleanValues(MethodCallExpression expression, boolean negate) {
        Boolean objBoolean = getBooleanValue(expression.getObjectExpression());
        Boolean argBoolean = getBooleanValue(expression.getArguments());
        if (objBoolean != null && argBoolean != null) {
            getContext().append(objBoolean.toString());
            getContext().append(" = ");
            getContext().append(argBoolean.toString());
            return true;
        }

        if (handleBooleanComparison(objBoolean, expression.getArguments(), negate)) {
            return true;
        } else {
            return handleBooleanComparison(argBoolean, expression.getObjectExpression(), negate);
        }
    }

    @Override
    protected String getOperator() {
        return "=";
    }

    private boolean isNotExpression(Expression expression) {
        if (expression instanceof NotExpression) {
            return true;
        } else if (expression instanceof TupleExpression) {
            return isNotExpression(((TupleExpression) expression).getExpression(0));
        } else {
            return false;
        }
    }

    private boolean handleBooleanComparison(Boolean booleanValue, Expression expression, boolean negate) {
        boolean notExpression = isNotExpression(expression);
        if (booleanValue != null && (!(expression instanceof PropertyExpression) || notExpression)) {
            Context c = getContext();
            if (notExpression) {
                c.parse(expression, (negate && booleanValue) || (!negate && !booleanValue), true);
                return true;
            }
            Class type = c.getTypeResolver().resolve(expression);
            if (ClassUtils.isAssignable(type, Boolean.class, true)) {
                c.append("(");
                c.parse(expression, (negate && booleanValue) || (!negate && !booleanValue), false);

                ParsableExpression parsableExpression = null;
                try {
                    parsableExpression = AbstractExpressionFactory.getFactory(expression).createParsableExpression(c, expression);
                } catch (Exception e) {
                }
                if (parsableExpression == null || parsableExpression.isNullCheckable()) {
                    c.append(negate ? " OR " : " AND ");
                    c.appendNullCheck(expression, !negate);
                }

                c.append(")");

                return true;
            }
        }
        return false;
    }
}

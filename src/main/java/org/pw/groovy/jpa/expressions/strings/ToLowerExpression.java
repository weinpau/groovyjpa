package org.pw.groovy.jpa.expressions.strings;

/**
 *
 * @author Paul Weinhold
 */
public class ToLowerExpression extends AbstractStringExpression {

    @Override
    protected String getOperator() {
        return "LOWER";
    }

    @Override
    public String getMethodName() {
        return "toLowerCase";
    }
}

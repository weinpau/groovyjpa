package org.pw.groovy.jpa.expressions.collections;

import org.codehaus.groovy.ast.expr.MethodCallExpression;
import org.pw.groovy.jpa.Context;
import org.pw.groovy.jpa.IdentificationVariableCreator;
import org.pw.groovy.jpa.TypeResolver;

/**
 *
 * @author Paul Weinhold
 */
public class AnyExpression extends CollectionExpression {

    @Override
    public String getMethodName() {
        return "any";
    }

    @Override
    public int getParameterCount() {
        return 0;
    }

    @Override
    public Class getResultType() {
        return Boolean.class;
    }

    @Override
    public void parse(MethodCallExpression expression, boolean negate, boolean appendCheck) {
        final String iv = IdentificationVariableCreator.createIdentificationVariable();
        TypeResolver typeResolver = getTypeResolver(expression);
        final Context c = getContext();
        final String selectParameter = getSelectParameter(iv, typeResolver);

        if (negate) {
            c.append("NOT ");
        }
        c.append("(EXISTS ");
        appendSubquery(typeResolver, iv, null, new Where() {
            @Override
            public void appendWhere() {
                c.append(selectParameter);
                c.append(" IS NOT NULL");
                if (!iv.equals(selectParameter)) {
                    c.append(" AND ");
                    c.append(selectParameter);
                    c.append(" <> FALSE AND ");
                    c.append(selectParameter);
                    c.append(" <> ''");
                }
            }
        });

        c.append(")");
    }

    @Override
    public boolean isNullCheckable() {
        return false;
    }
    
    
}

package org.pw.groovy.jpa.expressions;

import org.codehaus.groovy.ast.expr.Expression;
import org.pw.groovy.jpa.expressions.checkstrategies.CheckStrategy;

/**
 *
 * @author Paul Weinhold
 */
public interface Checkable<E extends Expression> {

    public CheckStrategy<E> getCheckStrategy();

    public void setCheckStrategy(CheckStrategy<E> checkStrategy);
}

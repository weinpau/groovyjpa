package org.pw.groovy.jpa.expressions.collections;

import org.codehaus.groovy.ast.expr.MethodCallExpression;

/**
 *
 * @author Paul Weinhold
 */
public abstract class AggregateExpression extends CollectionExpression {

    @Override
    public int getParameterCount() {
        return 0;
    }

    @Override
    public void parse(MethodCallExpression expression, boolean negate, boolean appendCheck) {
        appendAggregateFunction(expression, getFunction());
        if (appendCheck) {
            getCheckStrategy().appendCheck(getContext(), expression, negate);
        }
    }

    protected String getFunction() {
        return getMethodName().toUpperCase();
    }
}

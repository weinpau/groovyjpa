package org.pw.groovy.jpa.expressions;

import org.codehaus.groovy.ast.expr.ConstantExpression;
import org.pw.groovy.jpa.Context;
import org.pw.groovy.jpa.expressions.checkstrategies.CheckStrategy;

/**
 *
 * @author Paul Weinhold
 */
public class ParsableConstantExpression extends ParsableExpression<ConstantExpression> implements Checkable<ConstantExpression> {

    private CheckStrategy<ConstantExpression> checkStrategy;

    @Override
    public Class getResultType() {
        return Object.class;
    }

    @Override
    public void parse(ConstantExpression expression, boolean negate, boolean appendCheck) {
        Object value = expression.getValue();
        Context c = getContext();
        c.appendParameter(value);
        if (value != null && appendCheck) {
            getCheckStrategy().appendCheck(c, expression, negate);
        }
        if (value == null && appendCheck) {
            if (negate) {
                c.append("TRUE = TRUE");
            } else {
                c.append("TRUE = FALSE");
            }
        }
    }

    @Override
    public CheckStrategy<ConstantExpression> getCheckStrategy() {
        return checkStrategy;
    }

    @Override
    public void setCheckStrategy(CheckStrategy<ConstantExpression> checkStrategy) {
        this.checkStrategy = checkStrategy;
    }
}

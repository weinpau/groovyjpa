package org.pw.groovy.jpa.expressions.checkstrategies;

import groovy.lang.GroovyRuntimeException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import org.apache.commons.lang.ClassUtils;
import org.codehaus.groovy.ast.expr.Expression;
import org.pw.groovy.jpa.Context;

/**
 *
 * @author Paul Weinhold
 */
public final class CheckStrategyProvider {

    private static final Map<Class, Class<? extends CheckStrategy>> strategies = new HashMap<>();

    static {
        strategies.put(Object.class, ObjectCheck.class);
        strategies.put(Boolean.class, BooleanCheck.class);
        strategies.put(String.class, StringCheck.class);
        strategies.put(Number.class, NumberCheck.class);
    }

    private CheckStrategyProvider() {
    }

    public static <E extends Expression> CheckStrategy<E> getCheckStrategy(Context context, E expression) {
        Class type = context.getTypeResolver().resolve(expression);
        Class<? extends CheckStrategy> strategyClass = getStrategyClass(type);

        if (strategyClass != null) {
            try {
                return strategyClass.newInstance();
            } catch (InstantiationException | IllegalAccessException ex) {
            }
        }
        throw new GroovyRuntimeException();
    }

    private static Class<? extends CheckStrategy> getStrategyClass(Class type) {
        Class<? extends CheckStrategy> strategyClass = strategies.get(type);
        if (strategyClass == null) {
            for (Entry<Class, Class<? extends CheckStrategy>> entry : strategies.entrySet()) {
                if (ClassUtils.isAssignable(type, entry.getKey(), true)) {
                    strategyClass = entry.getValue();
                    break;
                }
            }
        }
        return strategyClass;
    }
}

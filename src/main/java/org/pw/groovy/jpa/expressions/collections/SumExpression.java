package org.pw.groovy.jpa.expressions.collections;

/**
 *
 * @author Paul Weinhold
 */
public class SumExpression extends AggregateExpression {

    @Override
    public Class getResultType() {
        return Number.class;
    }

    @Override
    public String getMethodName() {
        return "sum";
    }
}

package org.pw.groovy.jpa.expressions.strings;

import org.codehaus.groovy.ast.expr.MethodCallExpression;
import org.pw.groovy.jpa.Context;
import org.pw.groovy.jpa.expressions.ParsableMethodExpression;

/**
 *
 * @author Paul Weinhold
 */
public class ContainsExpression extends ParsableMethodExpression {

    @Override
    public String getMethodName() {
        return "contains";
    }

    @Override
    public int getParameterCount() {
        return 1;
    }

    @Override
    public Class getObjectType() {
        return String.class;
    }

    @Override
    public Class getResultType() {
        return Boolean.class;
    }

    @Override
    public void parse(MethodCallExpression expression, boolean negate, boolean appendCheck) {
        Context c = getContext();

        c.append("(LOCATE (");
        c.parse(expression.getArguments(), false, false);
        c.append(", ");
        c.parse(expression.getObjectExpression(), false, false);
        if (negate) {
            c.append(") = 0");
        } else {
            c.append(") > 0");
        }
        if (appendCheck) {
            c.append(negate ? " OR " : " AND ");
            c.appendNullCheck(expression.getObjectExpression(), !negate);
        }

        c.append(")");
    }
}

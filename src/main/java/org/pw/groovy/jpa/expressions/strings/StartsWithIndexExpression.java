package org.pw.groovy.jpa.expressions.strings;

import org.codehaus.groovy.ast.expr.ArgumentListExpression;
import org.codehaus.groovy.ast.expr.MethodCallExpression;
import org.pw.groovy.jpa.Context;

/**
 *
 * @author Paul Weinhold
 */
public class StartsWithIndexExpression extends StartsWithExpression {

    @Override
    public int getParameterCount() {
        return 2;
    }

    @Override
    public void parse(MethodCallExpression expression, boolean negate, boolean appendCheck) {
        Context c = getContext();
        ArgumentListExpression ale = (ArgumentListExpression) expression.getArguments();

        c.append("(LENGTH (");
        c.parse(expression.getObjectExpression(), false, false);
        c.append(") - ");
        c.parse(ale.getExpression(1), false, false);
        c.append(negate ? " < " : " >= ");
        c.append("LENGTH (");
        c.parse(ale.getExpression(0), false, false);
        c.append(") ");
        c.append(negate ? "OR" : "AND");
        c.append(" SUBSTRING (");
        c.parse(expression.getObjectExpression(), false, false);
        c.append(", 1 + ");
        c.parse(ale.getExpression(1), false, false);
        c.append(", LENGTH (");
        c.parse(ale.getExpression(0), false, false);
        c.append(")) ");
        c.append(negate ? "<>" : "=");
        c.append(" (");
        c.parse(ale.getExpression(0), false, false);
        c.append(")");

        if (appendCheck) {
            c.append(negate ? " OR " : " AND ");
            c.appendNullCheck(expression.getObjectExpression(), !negate);
        }
        c.append(")");
    }
}

package org.pw.groovy.jpa.expressions.strings;

/**
 *
 * @author Paul Weinhold
 */
public class ToUpperExpression extends AbstractStringExpression {

    @Override
    protected String getOperator() {
        return "UPPER";
    }

    @Override
    public String getMethodName() {
        return "toUpperCase";
    }
}

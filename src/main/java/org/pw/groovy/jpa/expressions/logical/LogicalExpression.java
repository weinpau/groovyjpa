package org.pw.groovy.jpa.expressions.logical;

import org.codehaus.groovy.ast.expr.MethodCallExpression;
import org.pw.groovy.jpa.expressions.ParsableMethodExpression;

/**
 *
 * @author Paul Weinhold
 */
public abstract class LogicalExpression extends ParsableMethodExpression {

   
    @Override
    public void parse(MethodCallExpression expression, boolean negate, boolean appendCheck) {
        getContext().parse(expression.getObjectExpression(), negate, appendCheck);
        getContext().append(" ");
        getContext().append(getOperator(negate));
        getContext().append(" ");
        getContext().parse(expression.getArguments(), negate, appendCheck);
    }

    @Override
    public int getParameterCount() {
        return 1;
    }

    @Override
    public Class getObjectType() {
        return Object.class;
    }

    @Override
    public Class getResultType() {
        return Boolean.class;
    }

    public abstract String getOperator(boolean negate);
}

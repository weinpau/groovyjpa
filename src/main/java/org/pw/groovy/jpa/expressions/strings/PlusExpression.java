package org.pw.groovy.jpa.expressions.strings;

import org.codehaus.groovy.ast.expr.Expression;
import org.codehaus.groovy.ast.expr.MethodCallExpression;
import org.pw.groovy.jpa.Context;
import org.pw.groovy.jpa.IdentificationVariableCreator;

/**
 *
 * @author Paul Weinhold
 */
public class PlusExpression extends AbstractStringExpression {

    @Override
    public int getParameterCount() {
        return 1;
    }

    @Override
    public Class getObjectType() {
        return Object.class;
    }

    @Override
    protected String getOperator() {
        return "";
    }

    @Override
    public String getMethodName() {
        return "plus";
    }

    @Override
    public void parse(MethodCallExpression expression, boolean negate, boolean appendCheck) {
        Context c = getContext();

        c.append("CONCAT (");
        extractString(expression.getObjectExpression());
        c.append(",");
        extractString(expression.getArguments());
        c.append(")");


        if (appendCheck) {
            getCheckStrategy().appendCheck(c, expression, negate);
        }
    }

    private void extractString(Expression expr) {
        Context c = getContext();
        if (c.isNullValue(expr)) {
            c.append("'null'");
        } else {
            c.append("(SELECT CASE WHEN ");
            c.appendNullCheck(expr, false);
            c.append(" THEN 'null' ELSE ");
            c.parse(expr, false, false);
            c.append(" END FROM ");
            String iv = IdentificationVariableCreator.createIdentificationVariable();
            c.appendEntity(c.getType());
            c.append(" ");
            c.append(iv);
            c.append(" WHERE ");
            c.append(iv);
            c.append(" = ");
            c.append(c.getIdentificationVariable());
            c.append(")");
        }
    }
}

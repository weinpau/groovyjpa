package org.pw.groovy.jpa.expressions.strings;

import groovy.lang.Range;
import org.codehaus.groovy.ast.expr.ArgumentListExpression;
import org.codehaus.groovy.ast.expr.ConstantExpression;
import org.codehaus.groovy.ast.expr.MethodCallExpression;
import org.codehaus.groovy.ast.expr.RangeExpression;
import org.pw.groovy.jpa.Context;

/**
 *
 * @author Paul Weinhold
 */
public class GetAtExpression extends CharAtExpression {

    @Override
    public String getMethodName() {
        return "getAt";
    }

    @Override
    public void parse(MethodCallExpression expression, boolean negate, boolean appendCheck) {
        Context c = getContext();
        ArgumentListExpression ale = (ArgumentListExpression) expression.getArguments();
        if (ale.getExpression(0) instanceof RangeExpression) {
            RangeExpression rangeExpression = (RangeExpression) ale.getExpression(0);
            handleRange(expression, rangeExpression, negate, appendCheck);
            return;
        }
        try {
            Class type = c.getTypeResolver().resolve(ale.getExpression(0));
            Range range = (Range) c.getClosureContext().evaluate(ale.getExpression(0));
            RangeExpression rangeExpression = new RangeExpression(new ConstantExpression(range.getFrom()), new ConstantExpression(range.getTo()), true);
            handleRange(expression, rangeExpression, negate, appendCheck);
            return;
        } catch (Exception e) {
        }

        super.parse(expression, negate, appendCheck);
    }

    private void handleRange(MethodCallExpression expression, RangeExpression range, boolean negate, boolean appendCheck) {
        Context c = getContext();
        c.append("(SUBSTRING (");
        c.parse(expression.getObjectExpression(), false, false);
        c.append(", (");
        c.parse(range.getFrom(), false, false);
        c.append(") + 1, ((");
        c.parse(range.getTo(), false, false);
        c.append(") - (");
        c.parse(range.getFrom(), false, false);
        c.append("))");
        if (range.isInclusive()) {
            c.append(" + 1");
        }
        c.append(")");
        if (appendCheck) {
            getCheckStrategy().appendCheck(c, expression, negate);
        }
        c.append(")");
    }
}

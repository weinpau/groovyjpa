package org.pw.groovy.jpa.expressions.arithmetic;

import groovy.lang.GroovyRuntimeException;
import org.codehaus.groovy.ast.expr.MethodCallExpression;
import org.pw.groovy.jpa.CodeVisitor;
import org.pw.groovy.jpa.Context;

/**
 *
 * @author Paul Weinhold
 */
public class PowerExpression extends ArithmeticExpression {

    @Override
    public String getMethodName() {
        return "power";
    }

    @Override
    public void parse(MethodCallExpression expression, boolean negate, boolean appendCheck) {
        Context c = getContext();
        Number exponent = (Number) c.getClosureContext().evaluate(expression.getArguments());
        if (exponent == null) {
            throw new GroovyRuntimeException("The exponent must be an constant number.");
        }

        try {
            CodeVisitor codeVisitor = new CodeVisitor();
            expression.getObjectExpression().visit(codeVisitor);
            if (!c.hasClosureParameter(codeVisitor.getVariables())) {
                Number value = (Number) c.getClosureContext().evaluate(codeVisitor.getCode());
                c.appendParameter(Math.pow(value.doubleValue(), exponent.doubleValue()));
                return;
            }
        } catch (Exception exp) {
        }
        if (exponent.doubleValue() % 1 != 0) {
            throw new GroovyRuntimeException("The exponent must be an even number.");
        }
        int intExponent = exponent.intValue();
        if (intExponent == 0) {
            c.append("1");
        } else {
            if (intExponent < 0) {
                c.append("(1 / ");
            }
            c.append("(");
            for (int i = 0; i < Math.abs(intExponent); i++) {
                c.parse(expression.getObjectExpression(), false, false);
                if (i + 1 < Math.abs(intExponent)) {
                    c.append(" * ");
                }
            }
            c.append(")");
            if (intExponent < 0) {
                c.append(")");
            }
        }

        if (appendCheck) {
            getCheckStrategy().appendCheck(c, expression, negate);
        }
    }
}

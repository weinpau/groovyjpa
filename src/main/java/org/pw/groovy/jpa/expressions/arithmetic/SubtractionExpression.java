package org.pw.groovy.jpa.expressions.arithmetic;

/**
 *
 * @author Paul Weinhold
 */
public class SubtractionExpression extends ArithmeticExpression {

    @Override
    protected String getOperator() {
        return "-";
    }

    @Override
    public String getMethodName() {
        return "minus";
    }
}

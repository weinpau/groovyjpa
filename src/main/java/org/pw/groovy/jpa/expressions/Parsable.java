package org.pw.groovy.jpa.expressions;

import org.codehaus.groovy.ast.expr.Expression;

/**
 *
 * @author Paul Weinhold
 */
public interface Parsable<E extends Expression> {

    public void parse(E expression, boolean negate, boolean appendCheck);
}

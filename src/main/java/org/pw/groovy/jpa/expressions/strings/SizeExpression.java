package org.pw.groovy.jpa.expressions.strings;

/**
 *
 * @author Paul Weinhold
 */
public class SizeExpression extends LengthExpression {

    @Override
    public String getMethodName() {
        return "size";
    }
}

package org.pw.groovy.jpa.expressions;

import org.codehaus.groovy.ast.expr.PropertyExpression;
import org.pw.groovy.jpa.Context;
import org.pw.groovy.jpa.expressions.checkstrategies.CheckStrategy;

/**
 *
 * @author Paul Weinhold
 */
public class ParsablePropertyExpression extends ParsableExpression<PropertyExpression> implements Checkable<PropertyExpression> {

    private CheckStrategy<PropertyExpression> checkStrategy;

    @Override
    public Class getResultType() {
        return Object.class;
    }

    @Override
    public void parse(PropertyExpression expression, boolean negate, boolean appendCheck) {
        String property = checkContext(getContext(), expression.getText());
        property = property.replaceAll("\\?", "");
        getContext().append(property);

        if (appendCheck) {
            getCheckStrategy().appendCheck(getContext(), expression, negate);
        }
    }

    private String checkContext(Context c, String text) {
        if (text.startsWith(c.getVariable())) {
            return text.replaceFirst(c.getVariable(), c.getIdentificationVariable());
        } else if (c.getParent() != null) {
            return checkContext(c.getParent(), text);
        }
        return text;
    }

    @Override
    public CheckStrategy<PropertyExpression> getCheckStrategy() {
        return checkStrategy;
    }

    @Override
    public void setCheckStrategy(CheckStrategy<PropertyExpression> checkStrategy) {
        this.checkStrategy = checkStrategy;
    }
}

package org.pw.groovy.jpa.expressions.collections;

import org.codehaus.groovy.ast.expr.MethodCallExpression;
import org.pw.groovy.jpa.Context;
import org.pw.groovy.jpa.IdentificationVariableCreator;
import org.pw.groovy.jpa.TypeResolver;

/**
 *
 * @author Paul Weinhold
 */
public class EveryClosureExpression extends EveryExpression {

    @Override
    public int getParameterCount() {
        return 1;
    }

    @Override
    public void parse(MethodCallExpression expression, boolean negate, boolean appendCheck) {

        Context c = getContext();
        if (negate) {
            c.append("NOT ");
        }
        c.append("(");

        TypeResolver typeResolver = getTypeResolver(expression);
        appendClosureSubquery(typeResolver, IdentificationVariableCreator.createIdentificationVariable(), expression.getArguments(), "COUNT");
        c.append(" = ");
        appendSubquery(typeResolver, IdentificationVariableCreator.createIdentificationVariable(), "COUNT", null);

        c.append(")");
    }
}

package org.pw.groovy.jpa.expressions.collections;

/**
 *
 * @author Paul Weinhold
 */
public class MinClosureExpression extends MinExpression {

    @Override
    public int getParameterCount() {
        return 1;
    }
}

package org.pw.groovy.jpa.expressions.strings;

import org.codehaus.groovy.ast.expr.MethodCallExpression;
import org.pw.groovy.jpa.Context;

/**
 *
 * @author Paul Weinhold
 */
public class CapitalizeExpression extends AbstractStringExpression {

    @Override
    public String getMethodName() {
        return "capitalize";
    }

    @Override
    public void parse(MethodCallExpression expression, boolean negate, boolean appendCheck) {
        Context c = getContext();
        c.append("CONCAT (SUBSTRING (UPPER (");
        c.parse(expression.getObjectExpression(), false, false);
        c.append("), 1, 1), SUBSTRING (");
        c.parse(expression.getObjectExpression(), false, false);
        c.append(", 2, LENGTH (");
        c.parse(expression.getObjectExpression(), false, false);
        c.append(") - 1))");

        if (appendCheck) {
            getCheckStrategy().appendCheck(c, expression, negate);
        }
    }
   
}

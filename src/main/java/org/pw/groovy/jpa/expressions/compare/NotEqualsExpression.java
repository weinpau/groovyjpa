package org.pw.groovy.jpa.expressions.compare;

import org.codehaus.groovy.ast.expr.MethodCallExpression;

/**
 *
 * @author Paul Weinhold
 */
public class NotEqualsExpression extends EqualsExpression {

    @Override
    public String getMethodName() {
        return "notEquals";
    }

    @Override
    public void parse(MethodCallExpression expression, boolean negate, boolean apppendTest) {
        super.parse(expression, !negate, apppendTest);
    }
}

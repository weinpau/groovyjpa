package org.pw.groovy.jpa.expressions.strings;

import org.codehaus.groovy.ast.expr.ArgumentListExpression;
import org.codehaus.groovy.ast.expr.MethodCallExpression;
import org.pw.groovy.jpa.Context;

/**
 *
 * @author Paul Weinhold
 */
public class SubstringEndIndexExpression extends SubstringExpression {

    @Override
    public int getParameterCount() {
        return 2;
    }

    @Override
    public void parse(MethodCallExpression expression, boolean negate, boolean appendCheck) {

        ArgumentListExpression ale = (ArgumentListExpression) expression.getArguments();
        Context c = getContext();
        c.append("SUBSTRING (");
        c.parse(expression.getObjectExpression(), false, false);
        c.append(", 1 + (");
        c.parse(ale.getExpression(0), false, false);
        c.append("), (");
        c.parse(ale.getExpression(1), false, false);
        c.append(") - (");
        c.parse(ale.getExpression(0), false, false);
        c.append("))");

        if (appendCheck) {
            getCheckStrategy().appendCheck(c, expression, negate);
        }
    }
}

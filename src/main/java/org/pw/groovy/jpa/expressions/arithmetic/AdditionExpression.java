package org.pw.groovy.jpa.expressions.arithmetic;

/**
 *
 * @author Paul Weinhold
 */
public class AdditionExpression extends ArithmeticExpression {

    @Override
    protected String getOperator() {
        return "+";
    }

    @Override
    public String getMethodName() {
        return "plus";
    }
}

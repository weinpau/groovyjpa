package org.pw.groovy.jpa.expressions.compare;

import org.codehaus.groovy.ast.expr.MethodCallExpression;

/**
 *
 * @author Paul Weinhold
 */
public class GreaterThanEqualExpression extends CompareExpression {

    @Override
    protected String getOperator() {
        return ">=";
    }

    @Override
    public String getMethodName() {
        return "greaterThanEqual";
    }

    @Override
    protected void nullCheck(MethodCallExpression methodCallExpression) {
        getContext().appendNullCheck(methodCallExpression.getArguments(), false);
    }
}

package org.pw.groovy.jpa.expressions;

import java.util.Map;
import org.codehaus.groovy.ast.expr.VariableExpression;
import org.pw.groovy.jpa.Context;
import org.pw.groovy.jpa.expressions.checkstrategies.CheckStrategy;

/**
 *
 * @author Paul Weinhold
 */
public class ParsableVariableExpression extends ParsableExpression<VariableExpression> implements Checkable<VariableExpression> {

    private CheckStrategy<VariableExpression> checkStrategy;

    @Override
    public Class getResultType() {
        return Object.class;
    }

    @Override
    public void parse(VariableExpression expression, boolean negate, boolean appendCheck) {
        Context c = getContext();
        Map<String, Object> localVariableContext = c.getClosureContext().getLocalVariableContext();
        if (localVariableContext.containsKey(expression.getName())) {
            Object value = localVariableContext.get(expression.getName());
            c.appendParameter(value);
        } else {
            checkContext(c, expression.getName());
        }
        
        if (appendCheck) {
            getCheckStrategy().appendCheck(c, expression, negate);
        }
    }

    private void checkContext(Context c, String var) {
        if (c.getVariable().equals(var)) {
            c.append(c.getIdentificationVariable());
        } else if (c.getParent() != null) {
            checkContext(c.getParent(), var);
        }
    }

    @Override
    public CheckStrategy<VariableExpression> getCheckStrategy() {
        return checkStrategy;
    }

    @Override
    public void setCheckStrategy(CheckStrategy<VariableExpression> checkStrategy) {
        this.checkStrategy = checkStrategy;
    }
}
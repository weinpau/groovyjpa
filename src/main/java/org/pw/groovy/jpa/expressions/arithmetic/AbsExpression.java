package org.pw.groovy.jpa.expressions.arithmetic;

import org.codehaus.groovy.ast.expr.MethodCallExpression;
import org.pw.groovy.jpa.Context;

/**
 *
 * @author Paul Weinhold
 */
public class AbsExpression extends ArithmeticExpression {

    @Override
    public String getMethodName() {
        return "abs";
    }

    @Override
    public int getParameterCount() {
        return 0;
    }

    @Override
    public void parse(MethodCallExpression expression, boolean negate, boolean appendCheck) {
        Context c = getContext();
        c.append("ABS (");
        c.parse(expression.getObjectExpression(), false, false);
        c.append(")");

        if (appendCheck) {
            getCheckStrategy().appendCheck(c, expression, negate);
        }
    }
}

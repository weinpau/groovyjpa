package org.pw.groovy.jpa.expressions.compare;

/**
 *
 * @author Paul Weinhold
 */
public class LessThanExpression extends CompareExpression {

    @Override
    public String getMethodName() {
        return "lessThan";
    }

    @Override
    protected String getOperator() {
        return "<";
    }

    @Override
    protected boolean notArgumentNull() {
       return true;
    }
    
    
    
}

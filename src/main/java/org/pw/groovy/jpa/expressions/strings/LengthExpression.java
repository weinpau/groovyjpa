package org.pw.groovy.jpa.expressions.strings;

/**
 *
 * @author Paul Weinhold
 */
public class LengthExpression extends AbstractStringExpression {

    @Override
    protected String getOperator() {
        return "LENGTH";
    }

    @Override
    public String getMethodName() {
        return "length";
    }

    @Override
    public Class getResultType() {
        return Integer.class;
    }
}

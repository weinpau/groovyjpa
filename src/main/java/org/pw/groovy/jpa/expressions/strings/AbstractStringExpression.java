package org.pw.groovy.jpa.expressions.strings;

import org.codehaus.groovy.ast.expr.MethodCallExpression;
import org.pw.groovy.jpa.Context;
import org.pw.groovy.jpa.expressions.Checkable;
import org.pw.groovy.jpa.expressions.ParsableMethodExpression;
import org.pw.groovy.jpa.expressions.checkstrategies.CheckStrategy;

/**
 *
 * @author Paul Weinhold
 */
public abstract class AbstractStringExpression extends ParsableMethodExpression implements Checkable<MethodCallExpression> {

    private CheckStrategy<MethodCallExpression> checkStrategy;

    @Override
    public int getParameterCount() {
        return 0;
    }

    @Override
    public Class getObjectType() {
        return String.class;
    }

    @Override
    public Class getResultType() {
        return String.class;
    }

    @Override
    public void parse(MethodCallExpression expression, boolean negate, boolean appendCheck) {
        Context c = getContext();
        c.append("(");
        c.append(getOperator());
        c.append(" (");
        c.parse(expression.getObjectExpression(), false, false);
        c.append(")");

        if (appendCheck) {
            getCheckStrategy().appendCheck(c, expression, negate);
        }
        c.append(")");
    }

    protected String getOperator() {
        return "";
    }

    @Override
    public CheckStrategy<MethodCallExpression> getCheckStrategy() {
        return checkStrategy;
    }

    @Override
    public void setCheckStrategy(CheckStrategy<MethodCallExpression> checkStrategy) {
        this.checkStrategy = checkStrategy;
    }
}

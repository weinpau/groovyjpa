package org.pw.groovy.jpa.expressions.strings;

import org.codehaus.groovy.ast.expr.MethodCallExpression;
import org.pw.groovy.jpa.Context;

/**
 *
 * @author Paul Weinhold
 */
public class CharAtExpression extends AbstractStringExpression {

    @Override
    public String getMethodName() {
        return "charAt";
    }

    @Override
    public int getParameterCount() {
        return 1;
    }

    @Override
    public void parse(MethodCallExpression expression, boolean negate, boolean appendCheck) {
        Context c = getContext();
        c.append("SUBSTRING (");
        c.parse(expression.getObjectExpression(), false, false);
        c.append(", (");
        c.parse(expression.getArguments(), false, false);
        c.append(") + 1, 1)");

        if (appendCheck) {
            getCheckStrategy().appendCheck(c, expression, negate);
        }
    }

}

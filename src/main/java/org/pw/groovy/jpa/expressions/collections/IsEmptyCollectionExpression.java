package org.pw.groovy.jpa.expressions.collections;

import org.codehaus.groovy.ast.expr.MethodCallExpression;
import org.pw.groovy.jpa.Context;
import org.pw.groovy.jpa.IdentificationVariableCreator;

/**
 *
 * @author Paul Weinhold
 */
public class IsEmptyCollectionExpression extends CollectionExpression {

    @Override
    public String getMethodName() {
        return "isEmpty";
    }

    @Override
    public int getParameterCount() {
        return 0;
    }

    @Override
    public Class getResultType() {
        return Boolean.class;
    }

    @Override
    public void parse(MethodCallExpression expression, boolean negate, boolean appendCheck) {
        Context c = getContext();
           c.append("(");
        appendAggregateFunction(expression, "COUNT");
        c.append(negate ? " > 0" : " = 0");
        if (appendCheck) {
            c.append(negate ? " OR " : " AND ");
            c.appendNullCheck(expression, !negate);

        }
        c.append(")");
    }
}

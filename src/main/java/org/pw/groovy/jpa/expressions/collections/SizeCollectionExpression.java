package org.pw.groovy.jpa.expressions.collections;

/**
 *
 * @author Paul Weinhold
 */
public class SizeCollectionExpression extends AggregateExpression {

    @Override
    public String getMethodName() {
        return "size";
    }

    @Override
    protected String getFunction() {
        return "COUNT";
    }

    @Override
    public Class getResultType() {
        return Long.class;
    }
}

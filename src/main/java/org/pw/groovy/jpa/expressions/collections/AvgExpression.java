package org.pw.groovy.jpa.expressions.collections;

/**
 *
 * @author Paul Weinhold
 */
public class AvgExpression extends AggregateExpression {

    @Override
    public Class getResultType() {
        return Double.class;
    }
    
    @Override
    public String getMethodName() {
        return "avg";
    }
}

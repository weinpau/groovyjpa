package org.pw.groovy.jpa.expressions.range;

import groovy.lang.Range;
import org.codehaus.groovy.ast.expr.ConstantExpression;
import org.codehaus.groovy.ast.expr.Expression;
import org.codehaus.groovy.ast.expr.MethodCallExpression;
import org.codehaus.groovy.ast.expr.RangeExpression;
import org.pw.groovy.jpa.Context;
import org.pw.groovy.jpa.expressions.ParsableMethodExpression;

/**
 *
 * @author Paul Weinhold
 */
public class ContainsWithinBoundsExpression extends ParsableMethodExpression {

    @Override
    public String getMethodName() {
        return "containsWithinBounds";
    }

    @Override
    public int getParameterCount() {
        return 1;
    }

    @Override
    public Class getObjectType() {
        return Range.class;
    }

    @Override
    public Class getResultType() {
        return Boolean.class;
    }

    @Override
    public void parse(MethodCallExpression expression, boolean negate, boolean appendCheck) {
        Context c = getContext();
        c.append("(");
        if (expression.getObjectExpression() instanceof RangeExpression) {
            RangeExpression rangeExpression = (RangeExpression) expression.getObjectExpression();
            handleRange(expression.getArguments(), rangeExpression, negate);
        } else {
            Range range = (Range) c.getClosureContext().evaluate(expression.getObjectExpression());
            RangeExpression rangeExpression = new RangeExpression(new ConstantExpression(range.getFrom()), new ConstantExpression(range.getTo()), true);
            handleRange(expression.getArguments(), rangeExpression, negate);
        }
        c.append(negate ? " OR " : " AND ");
        c.appendNullCheck(expression.getArguments(), !negate);
        c.append(")");

    }

    private void handleRange(Expression expression, RangeExpression range, boolean negate) {
        Context c = getContext();
        c.parse(expression, false, false);
        c.append(negate ? " < " : " >= ");
        c.parse(range.getFrom(), false, false);
        c.append(negate ? " OR " : " AND ");
        c.parse(expression, false, false);
        c.append(range.isInclusive() ? (negate ? " > " : " <= ") : (negate ? " >= " : " < "));
        c.parse(range.getTo(), false, false);
    }
}

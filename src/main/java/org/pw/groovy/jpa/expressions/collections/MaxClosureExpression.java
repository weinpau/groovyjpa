package org.pw.groovy.jpa.expressions.collections;

/**
 *
 * @author Paul Weinhold
 */
public class MaxClosureExpression extends MaxExpression {

    @Override
    public int getParameterCount() {
        return 1;
    }
}

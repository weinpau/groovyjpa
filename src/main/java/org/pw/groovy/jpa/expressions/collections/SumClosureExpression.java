package org.pw.groovy.jpa.expressions.collections;

/**
 *
 * @author Paul Weinhold
 */
public class SumClosureExpression extends SumExpression {

    @Override
    public int getParameterCount() {
        return 1;
    }
}

package org.pw.groovy.jpa.expressions.checkstrategies;

import org.codehaus.groovy.ast.expr.Expression;
import org.pw.groovy.jpa.Context;

/**
 *
 * @author Paul Weinhold
 */
public interface CheckStrategy<E extends Expression> {

    public void appendCheck(Context context, E expression, boolean negate);

}

package org.pw.groovy.jpa.expressions.arithmetic;

import org.codehaus.groovy.ast.expr.MethodCallExpression;
import org.pw.groovy.jpa.Context;
import org.pw.groovy.jpa.expressions.Checkable;
import org.pw.groovy.jpa.expressions.ParsableMethodExpression;
import org.pw.groovy.jpa.expressions.checkstrategies.CheckStrategy;

/**
 *
 * @author Paul Weinhold
 */
public abstract class ArithmeticExpression extends ParsableMethodExpression implements Checkable<MethodCallExpression> {

    private CheckStrategy<MethodCallExpression> checkStrategy;

    @Override
    public int getParameterCount() {
        return 1;
    }

    @Override
    public Class getObjectType() {
        return Number.class;
    }

    @Override
    public Class getResultType() {
        return Number.class;
    }

    @Override
    public void parse(MethodCallExpression expression, boolean negate, boolean appendCheck) {
        Context c = getContext();
        c.parse(expression.getObjectExpression(), false, false);
        c.append(" ");
        c.append(getOperator());
        c.append(" ");
        c.parse(expression.getArguments(), false, false);

        if (appendCheck) {
            getCheckStrategy().appendCheck(c, expression, negate);
        }

    }

    protected String getOperator() {
        return "";
    }

    @Override
    public void setCheckStrategy(CheckStrategy<MethodCallExpression> checkStrategy) {
        this.checkStrategy = checkStrategy;
    }

    @Override
    public CheckStrategy<MethodCallExpression> getCheckStrategy() {
        return checkStrategy;
    }
}

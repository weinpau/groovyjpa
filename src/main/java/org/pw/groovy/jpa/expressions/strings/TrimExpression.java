package org.pw.groovy.jpa.expressions.strings;

/**
 *
 * @author Paul Weinhold
 */
public class TrimExpression extends AbstractStringExpression {

    @Override
    protected String getOperator() {
        return "TRIM";
    }

    @Override
    public String getMethodName() {
        return "trim";
    }
}

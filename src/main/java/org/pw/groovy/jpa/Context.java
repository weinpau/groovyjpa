package org.pw.groovy.jpa;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.metamodel.Metamodel;
import org.codehaus.groovy.ast.expr.ConstantExpression;
import org.codehaus.groovy.ast.expr.Expression;
import org.codehaus.groovy.ast.stmt.Statement;
import org.pw.groovy.jpa.expressions.Parsable;
import org.pw.groovy.jpa.visitors.WhereVisitor;

/**
 *
 * @author Paul Weinhold
 */
public class Context implements Parsable<Expression> {

    private Context parent;
    private Class type;
    private String variable, identificationVariable, joins;
    private StringBuffer buffer = new StringBuffer();
    private Metamodel metamodel;
    private QueryParameters parameters;
    private ClosureContext closureContext;
    private TypeResolver typeResolver;

    public Context(ClosureContext closureContext, Class type, String identificationVariable, Metamodel metamodel, QueryParameters parameters) {
        this.type = type;
        this.identificationVariable = identificationVariable;
        this.metamodel = metamodel;
        this.parameters = parameters;
        this.closureContext = closureContext;
    }

    public Context(Context parent, Class type, String identificationVariable) {
        this.parent = parent;
        this.type = type;
        this.identificationVariable = identificationVariable;
    }

    public String getIdentificationVariable() {
        return identificationVariable;
    }

    public Class getType() {
        return type;
    }

    public String getVariable() {
        return variable;
    }

    public void setVariable(String variable) {
        this.variable = variable;
    }

    public ClosureContext getClosureContext() {
        return getRoot().closureContext;
    }

    public QueryParameters getQueryParameters() {
        return getRoot().parameters;
    }

    public Metamodel getMetamodel() {
        return getRoot().metamodel;
    }

    private StringBuffer getBuffer() {
        return buffer;
    }

    public Context getRoot() {
        if (parent != null) {
            return parent.getRoot();
        } else {
            return this;
        }

    }

    public Context getParent() {
        return parent;
    }

    public boolean isNullValue(Expression expression) {
        CodeVisitor codeVisitor = new CodeVisitor();
        expression.visit(codeVisitor);
        return !hasClosureParameter(codeVisitor.getVariables())
                && getClosureContext().evaluate(codeVisitor.getCode()) == null;
    }

    public void append(String token) {
        getBuffer().append(token);
    }

    public void appendEntity(Class entity) {
        append(getMetamodel().entity(entity).getName());
    }

    public void appendJoin(String join) {
        if (join == null) {
            return;
        }
        if (joins == null) {
            joins = "";
        }
        joins += join;
    }

    public void appendParameter(Object parameter) {
        if (parameter != null) {
            QueryParameters queryParameters = getQueryParameters();
            queryParameters.addParameter(parameter);
            append(queryParameters.getCurrentParamName());
        }
    }

    public void appendNullCheck(Expression expression, boolean not) {
        if (isNullValue(expression)) {
            if (not) {
                append("TRUE = FALSE");
            } else {
                append("TRUE = TRUE");
            }
        } else {
            parse(expression, false, false);
            append(" IS ");
            if (not) {
                append("NOT ");
            }
            append("NULL");
        }
    }

    private boolean isClosureParameter(String varName) {
        if (varName == null) {
            return false;
        }
        if (varName.equals(getVariable())) {
            return true;
        } else if (parent != null) {
            return parent.isClosureParameter(varName);
        } else {
            return false;
        }
    }

    public boolean hasClosureParameter(Collection<String> varNames) {
        for (String name : varNames) {
            if (isClosureParameter(name)) {
                return true;
            }
        }
        return false;
    }

    public void parse(Statement statement) {
        statement.visit(new WhereVisitor(this, false, true));
    }

    @Override
    public void parse(Expression expression, boolean negate, boolean appendCheck) {
        expression.visit(new WhereVisitor(this, negate, appendCheck));
    }

    public TypeResolver getTypeResolver() {
        if (typeResolver == null) {
            Map<String, Class> types = getClosureParameterTypes();
            types.putAll(getClosureContext().getLocalVariableContextTypes());
            typeResolver = new TypeResolver(types);
        }
        return typeResolver;
    }

    private Map<String, Class> getClosureParameterTypes() {
        Map<String, Class> types = null;
        if (parent == null) {
            types = new HashMap<>();
        } else {
            types = parent.getClosureParameterTypes();
        }
        types.put(variable, type);
        return types;
    }

    public String getJoins() {
        return joins;
    }

    public String getClause() {
        return buffer.toString();
    }
}
